# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Support module for handling Ansible variables.
This module knows nothing about semantics, it just marks certain dict keys
as "Ansible variables" and can serialize these variables from dict to JSON.

>>> test_spec = { \
        Av('resolver'): 'bind', \
        'run': [1, 2] \
    }
"""
import collections
import json


class Av(collections.UserString):
    """Dict key to be translated to Ansible variable (like -e key=value)"""

    def __repr__(self):
        return f"Av({repr(self.data)})"


def filter_avs(in_dict) -> dict:
    """
    Return filtered dictionary where the only allowed keys are Av instances.

    >>> filter_avs({1: 'omit', \
               'string': 'omit', \
               Av('stringify'): 'str', \
               Av(1): 1})
    {Av('stringify'): 'str', Av('1'): 1}
    """
    return dict(filter(lambda tup: isinstance(tup[0], Av), in_dict.items()))


def serialize(in_dict) -> str:
    """Return JSON-serialized representation of dictionary limited to keys
    which were Av instances. New keys are stringified.

    >>> serialize({1: 'omit', \
                   'string': 'omit', \
                   Av('stringify'): 'serialize str', \
                   Av(1): 'serialize int'})
    '{\\n    "1": "serialize int",\\n    "stringify": "serialize str"\\n}'
    """
    str_dict = {str(key): val for key, val in filter_avs(in_dict).items()}
    return json.dumps(str_dict, indent=4, allow_nan=False, sort_keys=True)
