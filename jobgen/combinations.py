#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Helper to generate cartesian product of test parameters specified as dict
of lists. Includes extra support for string templating and generators.

Multiple test runs can be specified using a single dictionary with lists:
>>> test_spec = { \
        'resolver': 'bind', \
        'version': ['v9.11.30', 'v9.11.31'], \
        'run': list(range(1, 2 + 1)) \
    }
>>> assert sorted(expand_spec(test_spec)) \
        == \
    [{'resolver': 'bind', 'run': 1, 'version': 'v9.11.30'}, \
     {'resolver': 'bind', 'run': 1, 'version': 'v9.11.31'}, \
     {'resolver': 'bind', 'run': 2, 'version': 'v9.11.30'}, \
     {'resolver': 'bind', 'run': 2, 'version': 'v9.11.31'}]

Values can be templated and reference other variables from the same test "instance":
>>> test_spec = { \
        'resolver': 'bind', \
        'run': [1, 2], \
        'output_directory': Template('/tmp/out/${resolver}-run${run}') \
    }
>>> assert sorted(expand_spec(test_spec)) == \
        [{'resolver': 'bind', 'run': 1, 'output_directory': '/tmp/out/bind-run1'}, \
         {'resolver': 'bind', 'run': 2, 'output_directory': '/tmp/out/bind-run2'}]

Callables in place of dict values are also supported, see eval_val().
"""

from typing import Any, Dict, Iterator, Mapping, List, Union
import functools
import collections
import itertools
import json
import string


def is_iterable(val) -> bool:
    """Only plain lists can be used to specify multiple variants"""
    return isinstance(val, list)


def eval_val(val, context: Dict):
    """
    Evaluate one level of Template or callables.
    Context is passed as first argument to callables and Template.eval_substitute.

    >>> context = {'int': 5, \
                   'callable': lambda ctx: ctx['int'] + 1, \
                   'rectempl': Template('recursive ${callable}')}

    Nothing is done to primitive types:
    >>> eval_val(123, context)
    123

    Templates can reference values inside the context:
    >>> eval_val(Template('foo ${callable} bar'), context)
    'foo 6 bar'
    >>> eval_val(context['rectempl'], context)
    'recursive 6'

    Return values from callables are returned as is:
    >>> eval_val(context['callable'], context)
    6

    Callables can also generate a new level of indirection:
    >>> two_levels = {'int': 5, \
                      'callable': lambda ctx: Template("stringified ${int}")}
    >>> assert isinstance(eval_val(two_levels['callable'], two_levels), Template)

    To evaluate the newly-generated indirection, call eval_str again:
    >>> eval_val(eval_val(two_levels['callable'], two_levels), two_levels)
    'stringified 5'
    """
    if isinstance(val, Template):
        return val.eval_substitute(context)
    if callable(val):
        return val(context)
    return val


@functools.total_ordering
class OrderableDict(collections.UserDict):
    """Hack to enable ordering, used by tests.

    >>> OrderableDict({1: 1, 2: 2}) < OrderableDict({1: 2, 2: 2})
    True
    """

    def __lt__(self, other) -> bool:
        """Compare tuples of sorted keys"""
        return tuple(sorted(self.items())) < tuple(sorted(other.items()))


class EvalDict(OrderableDict):
    """Dict to eval_val() retrieved values.
    >>> assert EvalDict({'i': 2}) \
                     == {'i': 2}

    References in templates or from callables are resolved to the dict itself:
    >>> assert EvalDict({'i': 2, \
                         'tmpl': Template('t${i}t'), \
                         'callable': lambda ctx: 'c' * ctx['i']}) \
                     == {'i': 2, 'tmpl': 't2t', 'callable': 'cc'}

    Callables have access to the original values,
    and return value of a callable is also left intact.
    >>> assert EvalDict({'a': 1, \
                         'b': 2, \
                         'sum': lambda ctx: ctx['a'] + ctx['b']}) \
                     == {'a': 1, \
                         'b': 2, \
                         'sum': 3}

    This enables callables to generate lists for further use.
    If string output is required use str() inside the callable.
    >>> assert EvalDict({'n': 3, \
                         'gen': lambda ctx: list(range(1, ctx['n'] + 1))}) \
                     == {'n': 3, \
                         'gen': [1, 2, 3]}
    """

    def __getitem__(self, key) -> str:
        return eval_val(self.data[key], self.data)


class Template(string.Template):
    """string.Template which can resolve references using EvalDict."""

    def eval_substitute(self, mapping):
        """see also EvalDict.__getitem__"""
        return super().substitute(EvalDict(mapping))

    def __repr__(self):
        return f"<Template {self.template}>"


def expand_spec(spec: Union[Mapping, EvalDict]) -> Iterator[OrderableDict]:
    """
    Yield all possible dict combinations based on cartesian product
    of all lists in the input dict/specification.
    """
    constants = []
    iterables = []
    for key in spec:
        if is_iterable(spec[key]):
            iterables.append(key)
        else:
            constants.append(key)

    constant_dict = {key: spec[key] for key in constants}
    # this works even with empty list of iterables
    # because itertools.product always returns at least one empty tuple
    for variant_values in itertools.product(*map(lambda key: spec[key], iterables)):
        variant_dict = constant_dict.copy()
        variant_dict.update(zip(iterables, variant_values))
        variant_eval = EvalDict(variant_dict)
        if any(map(callable, variant_dict.values())):
            # callables can generate new lists, recurse
            yield from expand_spec(variant_eval)
        else:
            # no callables -> no risk of generating incomplete dicts
            # forcefully trigger __getitem__ on all items to get them evaled,
            # but still use OrderableDict to get __lt__ defined for tests
            yield OrderableDict(variant_eval.items())  # type: ignore


def expand_multiple(spec_list: List[Dict]) -> List[OrderableDict]:
    """Expand all specifications in a list and merge all results into one list"""
    combinations = []  # type: List[OrderableDict]
    for spec in spec_list:
        combinations.extend(expand_spec(spec))
    return combinations


def to_json_file(combinations: List[OrderableDict], output_fname: str):
    """Write out list of individual combinations with keys stringified.

    Stringification enables us to use complex types as dict keys."""
    evaled = []
    for combination in combinations:
        # stringify keys
        # values are returned from EvalDict and should be be JSON-encondable
        evaled.append({str(k): v for k, v in combination.items()})
    with open(output_fname, "w", encoding="utf-8") as combf:
        json.dump(evaled, combf, indent=4)


def group_keys(
    combinations: List[OrderableDict], ignored_keys: frozenset[str]
) -> frozenset[str]:
    """
    Determine set of keys which have more than one value in given list of combinations.
    All keys and values must be hashable.
    """
    key2vals = {}  # type: Dict[str, Any]
    for variant in combinations:
        for key, val in variant.items():
            key2vals.setdefault(key, set()).add(val)

    multi_keys = frozenset(
        key for key, val in key2vals.items() if key not in ignored_keys and len(val) > 1
    )
    return multi_keys
