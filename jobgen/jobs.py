# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Specifications of a single job that can represent a test or a postprocessing task.

There are multiple types of jobs and based on their type, they may require
certain mandatory keys to be present.

- Job is a generic task to be derived from.
- ShellJob is a generic task to be derived from.
- AnsibleJob is a DNS Shotgun test which will be executed as an ansible playbook.
- AWSAnsibleJob is an ISC-specific DNS Shotgun test which will be executed in AWS.
- PostProcJob is an ISC-specific DNS Shotgun job for postprocessing the results.
"""
import collections
import shlex
from typing import List, Set

from . import ansiblevars


GIT_DIR = "/var/opt"
BIND9_SHOTGUN_CI_GIT_DIR = "."  # local copy provided by Gitlab
RESOLVER_BENCHMARKING_GIT_DIR = f"{GIT_DIR}/resolver-benchmarking"
SHOTGUN_GIT_DIR = f"{GIT_DIR}/shotgun"
RESMON_GIT_DIR = f"{GIT_DIR}/resource-monitor"


class Job:
    """
    Represents generic job to be executed.

    The derived classes can extend uniq_keys, extra_mandatory_keys,
    ansible_keys properties which are used by JobValidator.
    """

    def __init__(self, spec):
        self.spec = spec
        self.after_job = None

    @property
    def uniq_keys(self):
        """Names of keys which values must be unique across the entire job collection."""
        return set(["job_name"])

    @property
    def extra_mandatory_keys(self):
        """
        Names of keys which must be present in the job but aren't specified
        elsewhere (uniq, ansible).
        """
        return set()

    @property
    def ansible_keys(self):
        """Names of keys which must be ansible values."""
        return set()

    @property
    def mandatory_keys(self):
        """Generated list of mandatory keys that must be present.

        DO NOT override. If you need to add a mandatory key, see extra_mandatory_keys.
        """
        return (
            self.uniq_keys.copy()
            .union(self.extra_mandatory_keys)
            .union(self.ansible_keys)
        )


class ShellJob(Job):
    """Job which is executed as a series of shell commands."""

    # strings which are escaped using " instead of '
    quote_exceptions: Set[str] = set()

    @property
    def commands(self):
        """List of commands to be executed for this test."""
        raise NotImplementedError

    def _shell_escape_token(self, token):
        """
        shlex.quote with exceptions:
        Enclose tokens with special characters into ',
        except for allowed tokens which are escaped using ".
        """
        escaped = shlex.quote(token)
        for special in self.quote_exceptions:
            # shlex.quote already enclosed string in ', so we terminate
            # ', start ", insert string, close ", and start ' again
            escaped = escaped.replace(special, f"""'"{special}"'""")
        return escaped

    def _shell_escape_line(self, cmd: List) -> str:
        return " ".join(self._shell_escape_token(token) for token in cmd)

    @property
    def shell_lines(self):
        """List of shell-escaped lines to be executed for this test."""
        return [self._shell_escape_line(cmd) for cmd in self.commands]


class AnsibleJob(ShellJob):
    """Job that will be executed as an Ansible command."""

    quote_exceptions = set(
        [
            "$BIND_TEAM_API_TOKEN",
            "${BIND_TEAM_API_TOKEN}",
        ]
    )

    @property
    def uniq_keys(self):
        keys = super().uniq_keys
        keys.add("results_directory")
        return keys

    @property
    def ansible_keys(self):
        keys = super().ansible_keys
        keys.add("results_directory")
        return keys

    def __init__(self, spec):
        super().__init__(spec)
        vars_str = ansiblevars.serialize(spec)
        self.vars_filename = f'ansible-{spec["job_name"]}.json'
        with open(self.vars_filename, "w", encoding="utf-8") as jsonf:
            jsonf.write(vars_str)

    @property
    def commands(self) -> List[List[str]]:
        return [
            [
                "ansible-playbook",
                "-e",
                f"@{self.vars_filename}",
                "-e",
                "bind_token=gitlab-ci-token:$BIND_TEAM_API_TOKEN",
                "shotgun.yaml",
            ]
        ]


class AWSAnsibleJob(AnsibleJob):
    """Job that will be executed via Ansible in AWS."""

    quote_exceptions = AnsibleJob.quote_exceptions.union(
        set(
            [
                "$CI_PIPELINE_ID",
                "${CI_PIPELINE_ID}",
                "$CI_JOB_ID",
                "${CI_JOB_ID}",
                "$CI_JOB_TOKEN",
                "${CI_JOB_TOKEN}",
            ]
        )
    )

    def __init__(self, spec, azs: List[str]):
        super().__init__(spec)
        self.azs = azs

    @property
    def extra_mandatory_keys(self):
        keys = super().extra_mandatory_keys
        keys.add("CLEANUP_AFTER_MIN")
        return keys

    @property
    def ansible_cmd_prefix(self) -> List[str]:
        """Partial ansible command to be executed."""
        cmd_prefix = [
            f"{BIND9_SHOTGUN_CI_GIT_DIR}/aws/shotgun_aws.py",
            "--name-prefix=gitlab-pipe$CI_PIPELINE_ID-job$CI_JOB_ID",  # expanded by shell
            f'--remove-after-min={self.spec["CLEANUP_AFTER_MIN"]}',
        ]

        azs_args = []
        for az in self.azs:
            azs_args.extend(["--availability-zone", az])
        cmd_prefix.extend(azs_args)

        cmd_prefix.extend(
            [
                "--",
                "ansible-playbook",
                "-i",
                "ephemeral_hosts_shotgun",
                "--ssh-common-args",
                "-o StrictHostKeyChecking=no",
                "-e",
                f"@{self.vars_filename}",
                "-e",
                "bind_token=gitlab-ci-token:$BIND_TEAM_API_TOKEN",
            ]
        )
        return cmd_prefix


class AWSAnsibleTestJob(AWSAnsibleJob):
    """DNS Shotgun test that will be executed via Ansible in AWS."""

    @property
    def commands(self) -> List[List[str]]:
        shotgun_cmd = self.ansible_cmd_prefix
        shotgun_cmd.extend(
            [
                "--skip-tags",
                "condor",
                "--skip-tags",
                "tuning-post",
                "--skip-tags",
                "docker-env",
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/aws/pre-test.yaml",
                f"{RESOLVER_BENCHMARKING_GIT_DIR}/shotgun.yaml",
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/aws/post-test.yaml",
            ]
        )

        return [
            shotgun_cmd,
        ]


class AWSAnsibleBuildJob(AWSAnsibleJob):
    """Job that builds AWS ECR images for DNS Shotgun tests if they are not present in ECR."""

    @property
    def shell_lines(self) -> List[str]:
        # shell_lines is overloaded instead of commands in order to be able to use
        # shell syntax
        build_cmd = self.ansible_cmd_prefix
        build_cmd.extend(
            [
                "--tags",
                "docker-build",
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/aws/pre-test.yaml",
                f"{RESOLVER_BENCHMARKING_GIT_DIR}/shotgun.yaml",
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/aws/docker-push.yaml",
            ]
        )

        return [
            "aws ecr describe-images --repository-name=bind-shotgun-cache --image-ids="
            + self._shell_escape_token(f"imageTag={self.spec['image_tag']}")
            + " && exit 0",
            self._shell_escape_line(build_cmd),
        ]


class PostProcAfterJob(ShellJob):
    """After script for the postproc job."""

    @property
    def commands(self) -> List[List[str]]:
        return [
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/generate-chart-index.py",
            ],
        ]


class PostProcJob(ShellJob):
    """Post-processing job to interpret the test results."""

    def __init__(self, spec, baseline=None, sample=None):
        super().__init__(spec)
        self.baseline = baseline
        self.sample = sample
        self.after_job = PostProcAfterJob(self.spec)

    @property
    def commands(self) -> List[List[str]]:
        cmds = [
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "latency",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "latency",
                "--until-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "latency",
                "--since-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "latency",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "latency",
                "--until-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "latency",
                "--since-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "latency",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "latency",
                "--until-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "latency",
                "--since-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "latency",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "latency",
                "--until-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "latency",
                "--since-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "load",
                "latency",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "load",
                "latency",
                "--until-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "load",
                "latency",
                "--since-rel",
                "0.5",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "response-rates",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "response-rates",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "load",
                "response-rates",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "resmon",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "load",
                "resmon",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "version",
                "connections",
            ],
            [
                f"{BIND9_SHOTGUN_CI_GIT_DIR}/group-plot.py",
                "--merge",
                "run",
                "--separate-charts",
                "load",
                "connections",
            ],
        ]

        if self.baseline and self.sample:
            shotgun_eval_vars = filter(
                lambda item: item[0].startswith("SHOTGUN_EVAL"), self.spec.items()
            )
            eval_cmd = [f"{key}={value}" for key, value in shotgun_eval_vars]
            eval_cmd.extend(
                [
                    f"{BIND9_SHOTGUN_CI_GIT_DIR}/evaluate-results.py",
                    "--baseline",
                    f"{self.baseline}",
                    "--sample",
                    f"{self.sample}",
                    ".",
                ]
            )
            cmds.append(eval_cmd)

        return cmds


def validate_mandatory_keys(jobs):
    """
    Verify that all mandatory keys are present. Raise ValueError otherwise.
    """
    for job in jobs:
        missing_keys = job.mandatory_keys - set(job.spec.keys())
        if missing_keys:
            raise ValueError(
                f"job specification {job.spec} is missing "
                f"mandatory keys {missing_keys}"
            )


def validate_ansible_keys(jobs):
    """
    Verify that all ansible keys are available. Raise ValueError otherwise.
    """
    for job in jobs:
        not_avs = list(
            key
            for key in job.spec.keys()
            if (key in job.ansible_keys and not isinstance(key, ansiblevars.Av))
        )
        if not_avs:
            raise ValueError(
                f"keys {not_avs} are not Ansible variables "
                f"in job specification {job.spec}"
            )


def validate_uniq_keys(jobs):
    """
    Verify that all unique keys have a unique value across jobs. Raise ValueError otherwise.
    """
    uniq_vals = collections.defaultdict(set)
    for job in jobs:
        for key in job.uniq_keys:
            value = job.spec[key]
            if value in uniq_vals[key]:
                raise ValueError(f"value {value} on key {key} is not unique")
            uniq_vals[key].add(value)


def validate(jobs):
    """Validate a set of jobs. Raise ValueError if any job is invalid.

    >>> Av = ansiblevars.Av

    # Job is missing a required variable
    >>> validate([Job({})])
    Traceback (most recent call last):
    ValueError: job specification {} is missing ...

    # AnsibleJob is missing mandatory results_directory
    >>> validate([AnsibleJob({Av('job_name'): 'test1'})])
    Traceback (most recent call last):
    ValueError: job specification {Av('job_name'): 'test1'} is missing ...

    # some keys in AnsibleJob aren't ansible values
    >>> validate([AnsibleJob({'job_name': 'test1', 'results_directory': '/'})])
    Traceback (most recent call last):
    ValueError: keys ['results_directory'] are not Ansible variables in ...

    # keys are not unique across jobs
    >>> validate([Job({'job_name': 'test1'}), Job({'job_name': 'test1'})])
    Traceback (most recent call last):
    ValueError: value test1 on key job_name is not unique
    """
    validate_mandatory_keys(jobs)
    validate_uniq_keys(jobs)
    validate_ansible_keys(jobs)
