# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Job generators that create a sequence of commands / CI configuration based on
the provided job specifications.
"""
import copy
import random
import yaml

from . import jobs


class Generator:
    """Generator that validates jobs and serializes them into execution-ready format."""

    def __init__(self):
        self.build_jobs = []
        self.test_jobs = []
        self.postproc_jobs = []

    @property
    def all_jobs(self):
        """Both test and postproc jobs"""
        all_jobs = self.build_jobs.copy()
        all_jobs.extend(self.test_jobs)
        all_jobs.extend(self.postproc_jobs)
        return all_jobs

    def _serialize(self) -> str:
        raise NotImplementedError

    def _generate(self):
        raise NotImplementedError

    def generate(self) -> str:
        """Performs all necessary steps to create an execution-ready format."""
        jobs.validate(self.all_jobs)
        self._generate()
        return self._serialize()


class ShellScriptGenerator(Generator):
    """Generator that produces an executable shell script."""

    def __init__(self):
        super().__init__()
        self.script_lines = []

    def _generate(self):
        for job in self.all_jobs:
            self.script_lines.extend(job.shell_lines)

    def _serialize(self):
        return "\n".join(self.script_lines)


class GitlabCIGenerator(Generator):
    """Generator that creates .gitlab-ci.yml child pipeline for the jobs."""

    BUILD_JOB_TEMPLATE = {
        "stage": "build",
        "tags": ["linux-benchmarking"],
        "needs": [
            {
                "pipeline": "$PARENT_PIPELINE_ID",  # shell expansion
                "job": "resolver-shotgun-pipeline-generator",
            }
        ],
        "timeout": "1h",
        # delay test jobs in attempt to make VM provisioning in AWS more successful
        "when": "delayed",
        "start_in": "1 seconds",
    }
    TEST_JOB_TEMPLATE = {
        "stage": "test",
        "tags": ["linux-benchmarking"],
        "needs": [
            {
                "pipeline": "$PARENT_PIPELINE_ID",  # shell expansion
                "job": "resolver-shotgun-pipeline-generator",
            }
        ],
        "allow_failure": True,
        "artifacts": {
            "expire_in": "1 day",
            "when": "always",
            # do not store per-thread stats, we never look at them anyway
            "exclude": [
                "**/results-shotgun/data/*/*.json",
            ],
        },
        "timeout": "2h",
        # delay test jobs in attempt to make VM provisioning in AWS more successful
        "when": "delayed",
        "start_in": "1 seconds",
    }
    POSTPROC_JOB_TEMPLATE = {
        "artifacts": {
            "expire_in": "1 week",
            "paths": ["*"],
            "when": "always",
            "exclude": [
                "**/resmon-captured.json",
            ],
        },
        "needs": [
            {
                "pipeline": "$PARENT_PIPELINE_ID",  # shell expansion
                "job": "resolver-shotgun-pipeline-generator",
            }
        ],
        "stage": "postproc",
        "tags": ["linux-benchmarking"],
        "timeout": "1h",
    }
    GLOBAL_TEMPLATE = {
        "workflow": {"name": "$PIPELINE_NAME"},
        "image": "$SHOTGUN_CI_IMAGE",
        "stages": ["build", "test", "postproc"],
        "variables": {
            "ANSIBLE_FORCE_COLOR": "true",
            "ANSIBLE_STDOUT_CALLBACK": "gitlab_ci_unixy",
        },
    }

    def __init__(self):
        super().__init__()
        self.ci_dict = copy.deepcopy(self.GLOBAL_TEMPLATE)
        self.build_job_delay = 1

    def _generate_job(self, job, ci_job):
        ci_job["script"] = job.shell_lines
        if job.after_job is not None:
            ci_job["after_script"] = job.after_job.shell_lines
        name = job.spec["job_name"]
        assert name not in self.ci_dict, f"duplicated job name {name}"
        self.ci_dict[name] = ci_job

    def _generate_build_job(self, job):
        ci_job = copy.deepcopy(self.BUILD_JOB_TEMPLATE)
        ci_job["start_in"] = f"{self.build_job_delay} seconds"
        self.build_job_delay += 5
        self._generate_job(job, ci_job)

    def _generate_test_job(self, job):
        ci_job = copy.deepcopy(self.TEST_JOB_TEMPLATE)
        ci_job["artifacts"]["paths"] = [job.spec["results_directory"]]
        if "needs" in job.spec:
            ci_job["needs"].append(job.spec["needs"])
        delay = random.randint(1, 20)
        ci_job["start_in"] = f"{delay} seconds"
        self._generate_job(job, ci_job)

    def _generate_postproc_job(self, job):
        ci_job = copy.deepcopy(self.POSTPROC_JOB_TEMPLATE)
        ci_job["needs"].extend(
            [{"job": test_job.spec["job_name"]} for test_job in self.test_jobs]
        )
        self._generate_job(job, ci_job)

    def _generate(self):
        for job in self.build_jobs:
            self._generate_build_job(job)
        for job in self.test_jobs:
            self._generate_test_job(job)
        for job in self.postproc_jobs:
            self._generate_postproc_job(job)

    def _serialize(self):
        return yaml.safe_dump(self.ci_dict, width=9999999, indent=4)
