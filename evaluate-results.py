#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Script to evaluate results from a shotgun run.
"""

from abc import ABCMeta, abstractmethod
import argparse
from dataclasses import dataclass
import json
import logging
import math
import operator
import os
from pathlib import Path
import statistics
from typing import Any, Callable, Dict, List, Optional, Union
import sys


class Threshold:
    """A minimum and maximum limit for aggregate difference."""

    def __init__(self, name: str, default_min: float, default_max: float):

        def env_or_default(kind: str, default: float) -> float:
            env_val = os.getenv(f"SHOTGUN_EVAL_THRESHOLD_{name}_{kind}", "")
            try:
                return float(env_val)
            except ValueError:
                return default

        self.min = env_or_default("MIN", default_min)
        self.max = env_or_default("MAX", default_max)


SHOTGUN_DATA_MAX_VERSION = 20200527


def check_shotgun_data_version(shotgun_dict):
    """Verify the DNS Shotgun data format is supported by this script."""
    if shotgun_dict["version"] > SHOTGUN_DATA_MAX_VERSION:
        raise RuntimeError("❗️️ unsupported shotgun file format (version too new)")


@dataclass
class AggregateResult:
    """Result of a single comparison"""

    name: str
    diff: float
    threshold: Threshold
    baseline: Optional[float] = None
    sample: Optional[float] = None
    number_format: str = ",.2f"

    @property
    def passed(self) -> bool:
        """Boolean flag indicating whether diff falls within threshold"""
        return self.diff >= self.threshold.min and self.diff <= self.threshold.max

    @property
    def icon(self) -> str:
        """Icon representing the result"""
        if self.passed:
            return "👌"
        return "🚫"

    @property
    def ref_data_str(self) -> str:
        """String with sample and baseline data for logging purposes"""
        if self.sample is None or self.baseline is None:
            return ""
        return f" (sample: {self.sample_str} / baseline: {self.baseline_str})"

    @property
    def sample_str(self) -> str:
        """String of the sample value formatted for priting"""
        if self.sample is None:
            return ""
        return f"{self.sample:{self.number_format}}"

    @property
    def baseline_str(self) -> str:
        """String of the baseline value formatted for priting"""
        if self.baseline is None:
            return ""
        return f"{self.baseline:{self.number_format}}"

    @property
    def diff_str(self) -> str:
        """String of the difference between sample and baseline value formatted for priting"""
        return f"{self.diff:+5.1%}"

    def __str__(self) -> str:
        return f"\t{self.icon}: {self.name}: {self.diff_str}{self.ref_data_str}"

    def to_dict(self) -> Dict[str, Any]:
        """Dictionary representing the object, intended for JSON output"""
        return {
            "name": self.name,
            "diff": self.diff,
            "diff_str": self.diff_str,
            "sample": self.sample,
            "sample_str": self.sample_str,
            "baseline": self.baseline,
            "baseline_str": self.baseline_str,
            "icon": self.icon,
            "passed": self.passed,
        }


@dataclass
class AggregateError:
    """Error which happened during comparison"""

    name: str
    message: str

    def __str__(self) -> str:
        return f"❗️️ {self.name}: {self.message}"

    def to_dict(self) -> Dict[str, Any]:
        """Dictionary representing the object, intended for JSON output"""
        return {
            "name": self.name,
            "message": self.message,
        }


@dataclass
class Aggregate(metaclass=ABCMeta):
    """Abstract class used for comparison of baseline and sample values."""

    name: str
    function: Callable
    threshold: Threshold
    number_format: str = ",.2f"

    @abstractmethod
    def compare(self, baseline_values, sample_values) -> AggregateResult:
        """Compares sample values against baseline values"""
        raise NotImplementedError


class MetricAggregate(Aggregate):
    """Compare two sequences by applying a metrics function and diff the results."""

    def compare(self, baseline_values, sample_values) -> AggregateResult:
        baseline_metric = self.function(baseline_values)
        sample_metric = self.function(sample_values)
        diff = sample_metric / baseline_metric - 1
        return AggregateResult(
            name=self.name,
            diff=diff,
            threshold=self.threshold,
            baseline=baseline_metric,
            sample=sample_metric,
            number_format=self.number_format,
        )


class DiffAggregate(Aggregate):
    """Diff each value in two sequences and then apply a metrics function on the resulting diffs."""

    def compare(self, baseline_values, sample_values) -> AggregateResult:
        diff_values = [
            sample / baseline - 1
            for sample, baseline in zip(sample_values, baseline_values, strict=True)
        ]
        diff = self.function(diff_values)
        return AggregateResult(
            name=self.name,
            diff=diff,
            threshold=self.threshold,
            number_format=self.number_format,
        )


@dataclass
class SeriesCheckerResults:
    """Results and/or errors from multiple comparisons."""

    label: str
    results: List[AggregateResult]
    errors: List[AggregateError]

    @property
    def passed(self):
        """Overall result of all individual results."""
        return not self.errors and all(res.passed for res in self.results)

    @property
    def icon(self) -> str:
        """Icon representing the result"""
        if self.errors:
            return "❗️️"
        if self.passed:
            return "✅️"
        return "❌️"

    def to_dict(self) -> Dict[str, Any]:
        """Dictionary representing the object, intended for JSON output"""
        return {
            "label": self.label,
            "results": [res.to_dict() for res in self.results],
            "errors": [err.to_dict() for err in self.errors],
            "icon": self.icon,
            "passed": self.passed,
        }


@dataclass
class SeriesChecker(metaclass=ABCMeta):
    """Evaluate comparisons of sequences of sample values against baseline values."""

    label: str
    comparisons: List[Union[MetricAggregate, DiffAggregate]]
    length_tolerance: float = 0

    @classmethod
    @abstractmethod
    def transform(cls, data) -> List[float]:
        """Transform (sample or baseline) input data into a list of numbers for comparison."""

    def evaluate(self, baseline_data, sample_data) -> SeriesCheckerResults:
        """Run checks on sample values against the baseline values."""
        results: List[AggregateResult] = []
        errors: List[AggregateError] = []
        baseline_values = self.transform(baseline_data)
        sample_values = self.transform(sample_data)

        # check length of baseline vs sample sequences
        length_diff = math.fabs(1 - len(sample_values) / len(baseline_values))
        if length_diff > self.length_tolerance:
            error = AggregateError(
                self.label,
                f"different sequence lengths (baseline len: {len(baseline_values)}, sample len: {len(sample_values)})",
            )
            logging.error(error)
            errors.append(error)
            return SeriesCheckerResults(self.label, results, errors)

        logging.info("🔍 %s: evaluating...", self.label)
        for comparison in self.comparisons:
            result = comparison.compare(baseline_values, sample_values)
            results.append(result)
            logging.info(result)

        ret = SeriesCheckerResults(self.label, results, errors)
        if ret.passed:
            logging.info("%s %s: PASS", ret.icon, ret.label)
        else:
            logging.error("%s %s: FAIL", ret.icon, ret.label)

        return ret


@dataclass
class ResmonSeriesChecker(SeriesChecker):
    """Checker for resource-monitoring metrics."""

    length_tolerance: float = (
        0.02  # resmon sequences might have slight variations in number of samples
    )

    @classmethod
    def transform(cls, data):
        def pick_value(entry):
            value = entry[2]  # entries are tuples: (from, to, value)
            return value

        return list(map(pick_value, data))


def load_json(path: Path):
    """Load and parse JSON file."""
    with path.open("r", encoding="utf-8") as f:
        return json.load(f)


class ShotgunNoerrorRcodeRateSeriesChecker(SeriesChecker):
    """Check NOERROR rcode rate (relative to sent queries) in shotgun data."""

    @classmethod
    def transform(cls, data) -> List[float]:
        def noerror_rate(entry):
            if not entry["requests"]:
                return None
            return entry["rcode_noerror"] / entry["requests"]

        return [
            val for val in map(noerror_rate, data["stats_periodic"]) if val is not None
        ]


SHOTGUN_LATENCY_POINTS_OF_INTEREST = [
    1,
    2,
    5,
    10,
    20,
    50,
    100,
    200,
    500,
    1000,
    2000,
]  # ms


class ShotgunLatencyPercentilesSeriesChecker(SeriesChecker):
    """Check slowest percentiles in shotgun latency histogram."""

    @classmethod
    def transform(cls, data) -> List[float]:
        def slowest_percentile(latency_buckets):
            percentiles = [
                100.0,
            ]  # 0ms -> 100 % of answers take 0ms or more
            answers_all = sum(latency_buckets)
            answers_so_far = 0
            for i in range(1, len(latency_buckets)):
                answers_so_far += latency_buckets[i - 1]
                percentiles.append((answers_all - answers_so_far) / answers_all)
            return percentiles

        latencies = list(periodic["latency"] for periodic in data["stats_periodic"])
        latencies_sum = list(map(sum, zip(*latencies)))
        return slowest_percentile(latencies_sum)


class ShotgunLatencyOverallSeriesChecker(ShotgunLatencyPercentilesSeriesChecker):
    """Check "drift" of the latency histogram curve at logarithmic scale."""

    @classmethod
    def transform(cls, data) -> List[float]:
        percentiles = super().transform(data)
        return [percentiles[i] for i in SHOTGUN_LATENCY_POINTS_OF_INTEREST]


class DataSource(metaclass=ABCMeta):
    """Checker which evaluates multiple different comparisons of the same underlying data source."""

    def __init__(self, comparators: List[SeriesChecker]):
        self.comparators = comparators

    @abstractmethod
    def check(self, baseline_dir: Path, sample_dir: Path) -> List[SeriesCheckerResults]:
        """Run all checks on sample and baseline data."""
        raise NotImplementedError


class ResmonDataSource(DataSource):
    """Checker for resource-monitoring metrics."""

    @classmethod
    def read_shotgun_time_boundaries(cls, directory: Path):
        """Get DNS shotgun time boundaries in which the test took place."""
        # quick hack to determine x min/max - pick a random Shotgun JSON
        shotgun_dir = directory / "results-shotgun" / "data"
        try:
            shotgun_path = next(shotgun_dir.glob("*.json"))
        except StopIteration as ex:
            raise RuntimeError(
                f"❗️️ missing shotgun results file in {shotgun_dir}"
            ) from ex
        shotgun_dict = load_json(shotgun_path)
        check_shotgun_data_version(shotgun_dict)

        # offset for time 0
        since = shotgun_dict["stats_sum"]["since_ms"] / 1000
        until = shotgun_dict["stats_sum"]["until_ms"] / 1000
        return since, until

    @classmethod
    def parse_resmon_data(cls, file_path: Path, since: float, until: float):
        """Parse resource-monitoring data."""
        loaded = load_json(file_path)
        try:
            # Data structure of resmon files: {"$procid": "$stat": [data]}
            # The following unpacks the nested dictionaries to get to the data.
            # The keys are dynamic, but there's always just a single one in each
            # dictionary in the parsed resmon file.
            data = list(list(loaded.values())[0].values())[0]
        except (TypeError, KeyError) as ex:
            raise RuntimeError(
                f"❗️️ malformed resmon data format in {file_path}"
            ) from ex

        def not_before_or_after(entry):
            time_from = entry[0]
            time_to = entry[1]
            return time_from >= since and time_to <= until

        return list(filter(not_before_or_after, data))

    # pylint: disable=too-many-locals
    def check(self, baseline_dir: Path, sample_dir: Path) -> List[SeriesCheckerResults]:
        baseline_since, baseline_until = self.read_shotgun_time_boundaries(baseline_dir)
        sample_since, sample_until = self.read_shotgun_time_boundaries(sample_dir)

        results: List[SeriesCheckerResults] = []

        for comparator in self.comparators:
            fname = f"{comparator.label}.json"
            baseline_file = baseline_dir / "resmon-parsed" / fname
            sample_file = sample_dir / "resmon-parsed" / fname

            if not baseline_file.is_file() and not sample_file.is_file():
                logging.info(
                    "⏩️ %s: SKIP (sample and baseline data are missing)",
                    comparator.label,
                )
                continue
            if not baseline_file.is_file() or not sample_file.is_file():
                errors = []
                if not baseline_file.is_file():
                    error = AggregateError(comparator.label, "baseline data is missing")
                    logging.error(error)
                    errors.append(error)
                if not sample_file.is_file():
                    error = AggregateError(comparator.label, "sample data is missing")
                    logging.error(error)
                    errors.append(error)
                results.append(
                    SeriesCheckerResults(
                        label=comparator.label, results=[], errors=errors
                    )
                )
                continue

            baseline_data = self.parse_resmon_data(
                baseline_file, baseline_since, baseline_until
            )
            sample_data = self.parse_resmon_data(
                sample_file, sample_since, sample_until
            )

            results.append(comparator.evaluate(baseline_data, sample_data))

        return results


class ShotgunDataSource(DataSource):
    """Checker for DNS Shotgun results."""

    @classmethod
    def load_shotgun_data(cls, directory: Path):
        """Load DNS Shotgun data from its results directory."""
        shotgun_dir = directory / "results-shotgun" / "data"
        n_files = len(list(shotgun_dir.glob("*.json")))
        if n_files == 0:
            raise RuntimeError("❗️️ missing shotgun results file in {shotgun_dir}")
        if n_files > 1:
            raise NotImplementedError(
                "❗️️ multiple shotgun result files are unsupported"
            )

        shotgun_file = next(shotgun_dir.glob("*.json"))
        shotgun_dict = load_json(shotgun_file)
        check_shotgun_data_version(shotgun_dict)
        return shotgun_dict

    def check(self, baseline_dir: Path, sample_dir: Path) -> List[SeriesCheckerResults]:
        results: List[SeriesCheckerResults] = []
        baseline_data = self.load_shotgun_data(baseline_dir)
        sample_data = self.load_shotgun_data(sample_dir)
        for comparator in self.comparators:
            results.append(comparator.evaluate(baseline_data, sample_data))
        return results


RESMON_CHECKER = ResmonDataSource(
    [
        ResmonSeriesChecker(
            label="cpu.usage_percent.cg-docker",
            comparisons=[
                MetricAggregate(
                    name="median",
                    function=statistics.median,
                    threshold=Threshold("CPU", -0.05, 0.05),
                )
            ],
        ),
        ResmonSeriesChecker(
            label="memory.current-docker",
            comparisons=[
                MetricAggregate(
                    name="mean",
                    function=statistics.mean,
                    threshold=Threshold("MEMORY", -0.05, 0.05),
                    number_format=",.0f",
                )
            ],
        ),
    ]
)

# pylint: disable=unnecessary-direct-lambda-call
SHOTGUN_CHECKER = ShotgunDataSource(
    [
        ShotgunNoerrorRcodeRateSeriesChecker(
            label="noerror_rate",
            comparisons=[
                MetricAggregate(
                    name="mean",
                    function=statistics.mean,
                    threshold=Threshold("RCODE", -0.01, 0.01),
                    number_format=".2%",
                ),
            ],
        ),
        ShotgunLatencyPercentilesSeriesChecker(
            label="latency - slowest percentiles",
            comparisons=list(
                MetricAggregate(
                    name=f"{num} ms",
                    function=operator.itemgetter(num),
                    threshold=Threshold("LATENCY_PCTL", -0.2, 0.2),
                    number_format=".2%",
                )
                for num in SHOTGUN_LATENCY_POINTS_OF_INTEREST
            ),
        ),
        ShotgunLatencyOverallSeriesChecker(
            label="latency - log percentile drift",
            comparisons=[
                DiffAggregate(
                    name="mean diff",
                    function=statistics.mean,
                    threshold=Threshold("LATENCY_PCTL_DRIFT", -0.1, 0.1),
                    number_format=".2%",
                ),
            ],
        ),
    ]
)


def export_results(
    output_dir: Path,
    data: Dict[str, Any],
):
    """Export evaluation results to JSON."""
    json_path = output_dir / "evaluation.json"
    with json_path.open("w", encoding="utf-8") as f:
        json.dump(data, f)
    logging.info("⤵️ exported results to JSON: %s", json_path)


def check_all(directory: str, baseline: str, sample: str) -> bool:
    """Run all checks on the baseline and sample data."""
    output_dir = Path(directory).absolute()
    assert output_dir.is_dir(), f"{output_dir} is not a directory"

    baseline_dir = output_dir / baseline
    assert baseline_dir.is_dir(), f"{baseline_dir} is not a baseline directory"
    logging.info("📥 baseline dir: %s", baseline_dir)

    sample_dir = output_dir / sample
    assert sample_dir.is_dir(), f"{sample_dir} is not a sample directory"
    logging.info("📥 sample dir: %s", sample_dir)

    results = RESMON_CHECKER.check(baseline_dir, sample_dir)
    results.extend(SHOTGUN_CHECKER.check(baseline_dir, sample_dir))

    data = {
        "baseline": {
            "name": baseline,
            "path": str(baseline_dir),
        },
        "sample": {
            "name": sample,
            "path": str(sample_dir),
        },
        "results": [res.to_dict() for res in results],
    }
    export_results(output_dir, data)

    return all(result.passed for result in results)


def main():
    """Script entrypoint."""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)-15s %(levelname)8s %(message)s",
    )
    parser = argparse.ArgumentParser(
        description="""Compare new sample with DNS Shotgun and Resource Monitor results against a reference run.
\n\n
Expected directory structure:
- DIRECTORY/RUN-NAME/results-shotgun/data produced by DNS Shotgun.
- DIRECTORY/RUN-NAME/resmon-parsed with resource-monitor tool output.

Evaluation logic is hardcoded, but thresholds can be customized using SHOTGUN_EVAL_THRESHOLD_* env vars.""",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "--baseline",
        required=True,
        help="name of the baseline (reference) data subdirectory",
    )
    parser.add_argument(
        "--sample",
        required=True,
        help="name of the subdirectory with sample data to evaluate",
    )
    parser.add_argument(
        "directory", help="output directory with sample and baseline subdirectories"
    )
    args = parser.parse_args()
    res = check_all(args.directory, args.baseline, args.sample)
    if res:
        logging.info("✅️ all checks passed!")
    else:
        logging.error("❌️ some checks failed!")
        sys.exit(1)


if __name__ == "__main__":
    main()
