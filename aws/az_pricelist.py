#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Sort AWS availability zones by their current Spot instance price, cheapest first.
"""

import argparse
import datetime
import json
from typing import List, Dict, Any

import boto3

EC2_CLIENT = boto3.client("ec2")

# Availability Zones we can potentially run benchmarks in.
# AWS Region: us-east-1
AZS = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c",
    "us-east-1d",
    "us-east-1f",
]

INSTANCE_TYPE = "c5n.4xlarge"


def current_spot_prices(azs: List[str], instance_type: str) -> Dict[str, Any]:
    """
    Returns raw API response.
    """
    return EC2_CLIENT.describe_spot_price_history(
        Filters=[{"Name": "availability-zone", "Values": azs}],
        InstanceTypes=[instance_type],
        ProductDescriptions=["Linux/UNIX"],
        StartTime=datetime.datetime.now(),
    )


def azs_sorted_by_spot_price(azs: List[str], prices_raw: Dict[str, Any]) -> List[str]:
    """
    Returns new list sorted by Spot instance price.
    """
    az_prices = {
        azdata["AvailabilityZone"]: azdata["SpotPrice"]
        for azdata in prices_raw["SpotPriceHistory"]
    }
    return sorted(azs, key=lambda az: az_prices[az])


def to_json():
    """
    JSON representation of price-sorted list
    """
    current_prices = current_spot_prices(AZS, INSTANCE_TYPE)
    return json.dumps(azs_sorted_by_spot_price(AZS, current_prices))


def main():
    """
    script entrypoint
    """

    parser = argparse.ArgumentParser(
        description=(
            "Sort AWS availability zones by their current Spot instance "
            "price, cheapest first"
        )
    )
    parser.add_argument(
        "--s3-update", action="store_true", help="Update AZ pricelist in S3"
    )
    args = parser.parse_args()

    data = to_json()

    if args.s3_update:
        s3 = boto3.client("s3")
        s3.put_object(
            Body=data,
            Bucket="data.isc.org",
            ContentType="application/json",
            Key="bind9-resolver-benchmark/az-prices.json",
            Tagging="isc%3Aowner=pspacek%40isc.org&isc%3Agroup=bind9",
        )
    else:
        print(data)


if __name__ == "__main__":
    main()
