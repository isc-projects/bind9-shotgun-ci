#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Wrapper to create and fill named pipes for all objects in S3 bucket matching given key prefix.

Object download into newly created pipes starts immediatelly, and user-provided command is
executed as soon as all named pipes are created.

If an object name ends with COMPRESS_SUFFIXES then its content is passed through zstd -d
command and sent to an output pipe.

User-provided command should start reading from all pipes it needs right away otherwise
S3 might might terminate the connection because of timeout.

Requirements:
    - boto3 library
    - aws s3api command line interface
"""
import argparse
from pathlib import Path
import logging
import os
import sys
from typing import Iterator
import shutil
import subprocess
import tempfile

import boto3

COMPRESS_SUFFIXES = {
    "gz",
    "lz4",
    "lzma",
    "xz",
    "zst",
}
log = logging.getLogger("s3fifo")


def download_object_to_fifo(bucket: str, key: str, fspath: Path) -> subprocess.Popen:
    """Start asynchronous download into newly created named pipe."""
    assert fspath.is_absolute()
    os.mkfifo(fspath)
    # pylint: disable=consider-using-with
    cmd = ["aws", "s3api", "get-object", "--bucket", bucket, "--key", key, str(fspath)]
    logging.debug("executing %s", cmd)
    return subprocess.Popen(
        cmd,
        stdout=sys.stderr,
        stderr=sys.stderr,
        close_fds=True,
    )


def is_compressed(inpath: Path):
    """Path.suffix contains leading period like '.zst'"""
    return inpath.suffix[1:] in COMPRESS_SUFFIXES


def zstd_decompress(inpath: Path, outpath: Path):
    """
    Run asynchronous zstd -d on given files.
    """
    assert inpath.is_absolute()
    assert outpath.is_absolute()
    cmd = [
        "zstd",
        "--decompress",
        "--single-thread",
        "--no-progress",
        str(inpath),
        "-o",
        str(outpath),
    ]
    logging.debug("executing %s", cmd)
    return subprocess.Popen(
        cmd,
        stdout=sys.stderr,
        stderr=sys.stderr,
        close_fds=True,
    )


def list_object_keys(bucket: str, key_prefix: str) -> Iterator[str]:
    """Yield all keys with matching prefix."""
    s3cli = boto3.client("s3")

    paginator = s3cli.get_paginator("list_objects_v2")
    response_iterator = paginator.paginate(
        Bucket=bucket,
        Prefix=key_prefix,
    )

    for page in response_iterator:
        if page["KeyCount"] <= 0:
            raise RuntimeError(
                "S3 returned empty set of objects matching criteria, "
                "maybe a wrong prefix?"
            )
        for one_object in page["Contents"]:
            if one_object["Key"].endswith("/"):
                continue  # skip over directories
            yield one_object["Key"]


def main():
    """script entrypoint"""
    logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
    parser = argparse.ArgumentParser(
        description="Wrapper to create named pipes for objects in given S3 bucket and prefix. "
        "Command is executed after the directory is populated.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--bucket", type=str, default="data.isc.org", help=" ")
    parser.add_argument(
        "--key-prefix",
        type=str,
        default="bind9-resolver-benchmark/tn/8k/15m/",
        help="S3 key prefix to filter objets in bucket on",
    )
    parser.add_argument(
        "--outdir",
        type=Path,
        required=True,
        help="Directory to create named pipes in; must not exist",
    )
    parser.add_argument("command", type=str, nargs="+", help="command to be executed")
    args = parser.parse_args()

    # must not exist, this is to ensure we do not mix old/orphan pipes with fresh ones
    args.outdir.mkdir(parents=True)

    bg_jobs = []
    exit_code = 1
    try:
        for objkey in list_object_keys(args.bucket, args.key_prefix):
            filename = Path(objkey).name
            if is_compressed(Path(filename)):
                # download to a FIFO connected to zstd input
                compressedpath = Path(tempfile.mktemp(suffix=filename))
                # remove .zstd and similar suffixes
                outpath = (args.outdir / filename).with_suffix("")
                log.info(
                    "%s %s -> %s | zstd -d > %s",
                    args.bucket,
                    objkey,
                    compressedpath,
                    outpath,
                )
                os.mkfifo(outpath)
                bg_jobs.append(
                    download_object_to_fifo(args.bucket, objkey, compressedpath)
                )
                bg_jobs.append(zstd_decompress(compressedpath, outpath))
            else:
                outpath = args.outdir / filename
                log.info("%s %s -> %s", args.bucket, objkey, outpath)
                bg_jobs.append(download_object_to_fifo(args.bucket, objkey, outpath))

        # wrapper did it's job, run user-specified command
        log.info("executing %s", args.command)
        ret = subprocess.run(args.command, check=False)
        log.info("command %s exited with %d", ret.args, ret.returncode)
        exit_code = ret.returncode
    finally:
        # terminate AWS s3api subprocesses
        for proc in bg_jobs:
            proc.poll()
            if proc.returncode is not None:
                logging.debug("command %s exited with %d", proc.args, proc.returncode)
            else:
                logging.debug("killing %s", proc.args)
                proc.kill()
        shutil.rmtree(args.outdir)
    sys.exit(exit_code)


if __name__ == "__main__":
    main()
