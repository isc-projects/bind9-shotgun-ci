# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Remove AWS EC2 instances, placement groups, and SSH keypairs with tag
isc:remove_after containing timestamp in the past.
"""

import datetime
import logging
import sys
from typing import Dict, Iterator

import boto3
from botocore.exceptions import ClientError

EC2_CLIENT = boto3.client("ec2")


def now() -> datetime.datetime:
    """UTC datetime object"""
    return datetime.datetime.now(datetime.timezone.utc)


def is_expired_str(deadline: str) -> bool:
    """Is ISO-formated deadline in the past?"""
    return datetime.datetime.fromisoformat(deadline) < now()


def ephemeral_instances() -> Iterator[Dict]:
    """Yield EC2 instance descriptions:
    only running or stopped instances with isc:remove_after tag"""
    filters = [
        {"Name": "tag-key", "Values": ["isc:remove_after"]},
        {"Name": "instance-state-name", "Values": ["running", "stopped"]},
    ]
    paginator = EC2_CLIENT.get_paginator("describe_instances")
    for page in paginator.paginate(Filters=filters):
        for reservation in page["Reservations"]:
            yield from reservation["Instances"]


def ephemeral_placement_groups() -> Iterator[Dict]:
    """Yield EC2 placement group descriptions: only available groups with isc:remove_after tag"""
    filters = [
        {"Name": "tag-key", "Values": ["isc:remove_after"]},
        {"Name": "state", "Values": ["available"]},
    ]
    yield from EC2_CLIENT.describe_placement_groups(Filters=filters)["PlacementGroups"]


def ephemeral_key_pairs() -> Iterator[Dict]:
    """Yield EC2 key pair descriptions: only keys with isc:remove_after tag"""
    filters = [{"Name": "tag-key", "Values": ["isc:remove_after"]}]
    yield from EC2_CLIENT.describe_key_pairs(Filters=filters)["KeyPairs"]


def tags_expired(tags_list) -> bool:
    """Does the tag list point to an expired object?
    Helper to find the right tags in Boto3 tag description."""
    tags_dict = {item["Key"]: item["Value"] for item in tags_list}
    # we intentionally do not handle missing value to make sure
    # it blows up when the filter does not work
    return is_expired_str(tags_dict["isc:remove_after"])


def main():
    """Remove resources tagged as expired. Exit with non-zero exit code if an error occured.
    Run again later if an error occured."""
    logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
    errors = 0

    for inst in ephemeral_instances():
        iid = inst["InstanceId"]
        if tags_expired(inst["Tags"]):
            logging.info("terminating expired instance %s: %s", iid, inst)
            # if request to terminate multiple instances fails none of the instances are terminated
            # so we have to go one by one
            try:
                EC2_CLIENT.terminate_instances(InstanceIds=[iid])
            except ClientError as ex:
                errors += 1
                logging.error("error while terminating instance %s: %s", iid, ex)

    for plgrp in ephemeral_placement_groups():
        if tags_expired(plgrp["Tags"]):
            plgrpname = plgrp["GroupName"]
            logging.info("deleting expired placement group %s: %s", plgrpname, plgrp)
            try:
                EC2_CLIENT.delete_placement_group(GroupName=plgrpname)
            except ClientError as ex:
                errors += 1
                logging.error(
                    "error while deleting placement group %s: %s", plgrpname, ex
                )

    for keyp in ephemeral_key_pairs():
        if tags_expired(keyp["Tags"]):
            kpid = keyp["KeyPairId"]
            logging.info("deleting expired key pair %s: %s", kpid, keyp)
            try:
                EC2_CLIENT.delete_key_pair(KeyPairId=kpid)
            except ClientError as ex:
                errors += 1
                logging.error("error while deleting key pair %s: %s", kpid, ex)

    if errors:
        logging.error("%d errors occured", errors)
        sys.exit(1)


if __name__ == "__main__":
    main()
