#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Helper to start and cleanup VMs in Amazon EC2 and to generate Ansible
inventory and variables suitable for DNS Shotgun tests orchestrated
by playbook https://gitlab.nic.cz/knot/resolver-benchmarking/.
"""

import argparse
import copy
import ipaddress
import itertools
import logging
import os
from pathlib import Path
import subprocess
import sys
import time
from typing import Any, Dict
import uuid

import boto3
from botocore.config import Config
from botocore.exceptions import ClientError

# pylint: disable=wrong-import-position
sys.path.insert(0, str(Path(__file__).resolve().parent))
import isc_aws

INVENTORY_NAME = "ephemeral_hosts_shotgun"

VM_USER = "arch"

# Ansible inventory for https://gitlab.nic.cz/knot/resolver-benchmarking/
INVENTORY_TEMPLATE = """
[iterator]
server ansible_host={server_ip} ansible_user={user} ansible_ssh_private_key_file={key_path} ansible_python_interpreter=/usr/bin/python3

[shotgun]
client ansible_host={client_ip} ansible_user={user} ansible_ssh_private_key_file={key_path} ansible_python_interpreter=/usr/bin/python3
"""

# temporary volume for preprocessed PCAPs, to be attached only to the client machine
PCAP_STORAGE = {
    "BlockDeviceMappings": [
        {
            "DeviceName": "/dev/sdz",
            "Ebs": {
                "DeleteOnTermination": True,
                "VolumeType": "gp3",
                "VolumeSize": 40,  # GB
            },
        }
    ]
}

SSH_TIMEOUT_TOTAL = 120  # seconds to wait for VM to become reachable

TAGS = [
    {"Key": "isc:cost", "Value": "bind9benchmarking"},
    {"Key": "isc:environment", "Value": "development"},
    {"Key": "isc:group", "Value": "bind9"},
    {"Key": "isc:owner", "Value": "pspacek@isc.org"},
    {"Key": "isc:provisioning", "Value": "shotgun_aws.py"},
    {"Key": "isc:team", "Value": "bind9"},
]

# values required by playbook https://gitlab.nic.cz/knot/resolver-benchmarking/
# number of CPUs, network interfaces etc. have to match LaunchTemplate used below
VARS_TEMPLATE = """---
auto_docker_setup: False
cpu_cores: 8
cpu_threads: 16
srv_threads: 16
shotgun_server_ip6: "{local_ip}"
shotgun_bind_net6: "{shotgun_net}"
net_interfaces:
  - ens5
user_name: {user}
remote_user: {user}
work_dir: /tmp/workdir
"""

# common VM parameters are in LaunchTemplate - editable in AWS EC2 Console
VM_PARAMS = {
    "LaunchTemplate": {
        "LaunchTemplateId": "lt-00fe4fc6fb98c6c7f",
    },
}  # type: Dict[str, Any]

# Availability Zone (data centre) is selected based on subnet
# Subnets with access to the Internet
AZ_PARAMS = {
    "us-east-1a": {
        "SubnetId": "subnet-01b17c6f503b695c4",
    },
    "us-east-1b": {
        "SubnetId": "subnet-086463536fd544830",
    },
    "us-east-1c": {
        "SubnetId": "subnet-0d8a2a4d102e55e77",
    },
    "us-east-1d": {
        "SubnetId": "subnet-05eb37559708124ff",
    },
    "us-east-1f": {
        "SubnetId": "subnet-0da0c82046c1fb862",
    },
}

# Availability zone IDs sorted by Spot c5n.4xlarge instance prices
AZ_PREFERENCE = ["us-east-1f", "us-east-1a", "us-east-1d", "us-east-1b", "us-east-1c"]
assert set(AZ_PREFERENCE) == set(AZ_PARAMS)

# Security Group: allow everything out, everything inside VPC, limited SSH from outside
NIC_PARAMS: Dict[str, Any] = {
    "Groups": ["sg-0a65dcfc7e700d42b"],
}


def gen_inet_nic(azid: str):
    """define an Internet-facing network interface for run_instances request"""
    inet_nic = NIC_PARAMS.copy()
    inet_nic.update(AZ_PARAMS[azid])
    inet_nic["DeviceIndex"] = 0
    inet_nic["AssociatePublicIpAddress"] = True
    inet_nic["Ipv6PrefixCount"] = 1
    return inet_nic


def shotgun_prefix(orig: ipaddress.IPv6Network) -> ipaddress.IPv6Network:
    """shorten /80 to first /120, which is what we use for DNS Shotgun client"""
    SHOTGUN_PREFIXLEN = 120
    for short_net in orig.subnets(prefixlen_diff=SHOTGUN_PREFIXLEN - orig.prefixlen):
        return short_net
    raise RuntimeError()


def get_an_ip(subnet: ipaddress.IPv6Network) -> ipaddress.IPv6Address:
    """
    Return _an_ IP address from given IPv6 subnet.
    Under Python 3.11 it returns first one, but it is not guaranteed.
    """
    for ip in subnet.hosts():
        return ip
    raise RuntimeError()


def gen_ansible_files(client_inst, server_inst, key_path: str) -> None:
    """Write Ansible inventory and variable files for provided Boto3 ec2.Instance objects.
    "Client" VM is used to execute DNS Shotgun and thus needs PCAP_STORAGE.
    Resolver runs on "server". Writes into current working directory."""
    # IPv6 prefix delegations must already be in place, and we assume that
    # first couple addresses is actually configured on the interface
    server_prefix = shotgun_prefix(isc_aws.get_instance_ipv6_prefix(server_inst))
    server_ip = get_an_ip(server_prefix)
    client_prefix = shotgun_prefix(isc_aws.get_instance_ipv6_prefix(client_inst))
    client_ip = get_an_ip(client_prefix)
    identifiers = {"client": client_ip, "server": server_ip}

    os.makedirs("host_vars", exist_ok=True)
    # between test VMs
    for name, ipaddr in identifiers.items():
        Path(f"host_vars/{name}").write_text(
            VARS_TEMPLATE.format(
                local_ip=ipaddr,
                shotgun_net=client_prefix.with_prefixlen,
                user=VM_USER,
            ),
            encoding="utf-8",
        )

    # for Ansible controller
    Path(INVENTORY_NAME).write_text(
        INVENTORY_TEMPLATE.format(
            client_ip=client_ip,
            server_ip=server_ip,
            key_path=key_path,
            user=VM_USER,
        ),
        encoding="utf-8",
    )
    # debug
    logging.info(
        "ssh -i %s %s@%s  # client %s",
        key_path,
        VM_USER,
        client_ip,
        client_inst.id,
    )
    logging.info(
        "ssh -i %s %s@%s  # server %s",
        key_path,
        VM_USER,
        server_ip,
        server_inst.id,
    )


def ansible_ping() -> None:
    """Execute `ansible -m ping` once. Raises if ping fails."""
    subprocess.check_call(
        [
            "ansible",
            "-o",
            "-i",
            INVENTORY_NAME,
            # ansible command requires double quoting even though
            # -playbook command does not require it
            "--ssh-common-args",
            '"-o StrictHostKeyChecking=no"',
            "-m",
            "ping",
            "all",
        ]
    )


def wait_for_ansible_ping(timeout_s: int) -> None:
    """Repeat ansible -m ping until it works or RuntimeError is raised on timeout."""
    start = time.monotonic()
    deadline = start + timeout_s
    while time.monotonic() < deadline:
        try:
            logging.info("Ansible ping ...")
            ansible_ping()
            return
        except subprocess.CalledProcessError:
            continue
    raise RuntimeError("timeout while waiting for Ansible ping")


def random_uuid_namer(name_prefix, extra_tags):
    """Get Name with random UUID appended to distinguish individual runs"""
    randname = f"{name_prefix}-{uuid.uuid4()}"
    return isc_aws.Namer(randname, tags=TAGS + extra_tags)


# pylint: disable=too-many-arguments
def try_cluster(
    args,
    ec2,
    azid: str,
    namer: isc_aws.Namer,
    keyp: isc_aws.EphemeralKeypairInFile,
    spot: bool,
):
    """
    Attempt one spot or on-demand provisioning cycle in given AZ.
    Call Ansible if provisioning worked.
    Raises ClientError if AWS API calls fail.
    Raises subprocess.CalledProcessError if Ansible fails.
    Returns nothing, results are in files on disk.
    """
    with isc_aws.EphemeralCluster(namer, args.skip_removal_on_exit) as cluster:
        server_params = VM_PARAMS.copy()
        server_params.update({"NetworkInterfaces": [gen_inet_nic(azid)]})
        server_params.update(keyp.instance_params)
        server_inst = cluster.create_instances(
            "-server", create_params=server_params, spot=spot, ec2=ec2
        )[0]

        client_params = VM_PARAMS.copy()
        client_params.update({"NetworkInterfaces": [gen_inet_nic(azid)]})
        client_params.update(keyp.instance_params)
        client_params.update(PCAP_STORAGE)
        client_inst = cluster.create_instances(
            "-client", create_params=client_params, spot=spot
        )[0]

        cluster.wait_for_startup()

        for inst in [server_inst, client_inst]:
            logging.info(
                "applying workaround: add an extra IPv6 address to instance %s",
                inst.id,
            )
            addr = isc_aws.add_instance_ipv6_addr(inst)
            logging.info("new IPv6 address: %s", addr)

        gen_ansible_files(client_inst, server_inst, key_path=keyp.keyfile_path)
        logging.info("attempting to SSH into instances")
        wait_for_ansible_ping(SSH_TIMEOUT_TOTAL)
        logging.info("instances are reachable, fire!")
        subprocess.check_call(args.cmd)


def log_init():
    """Color logs are optional"""
    try:
        import coloredlogs  # pylint: disable=import-outside-toplevel

        field_styles = copy.deepcopy(coloredlogs.DEFAULT_FIELD_STYLES)
        field_styles["asctime"] = {}
        field_styles["levelname"] = {}
        coloredlogs.install(
            level=logging.INFO,
            # coloredlogs's asctime does not include msecs by default
            fmt="%(asctime)-15s,%(msecs)03d %(levelname)s %(message)s",
            field_styles=field_styles,
        )
    except ImportError:
        logging.basicConfig(
            level=logging.INFO, format="%(asctime)-15s %(levelname)s %(message)s"
        )


def parse_args():
    """CLI"""
    parser = argparse.ArgumentParser(
        description="Start two VMs in AWS, generate Ansible inventory, and execute command"
    )
    parser.add_argument(
        "--name-prefix",
        type=str,
        required=True,
        help="name prefix for ephemeral AWS objects (VMs, keys, ...)",
    )
    parser.add_argument(
        "--remove-after-min",
        type=int,
        required=True,
        help="tag for termination after this many minutes after start, "
        "even if this script is terminated",
    )
    parser.add_argument(
        "--skip-removal-on-exit",
        action="store_true",
        help="do not terminate VMs automatically on script exit; "
        "AWS resources will be cleaned up only after --remove-after-min time expires",
    )
    parser.add_argument(
        "--availability-zone",
        type=str,
        action="append",
        choices=sorted(AZ_PARAMS),
        help="AZ to try",
    )

    parser.add_argument(
        "cmd", type=str, nargs="+", help="command to be executed when VMs are ready"
    )
    return parser.parse_args()


def main():
    """
    Parameter --remove-after-min is mandatory to avoid accidental VM accumulation,
    e.g. when Gitlab CI job is terminated so this script does not have change to do cleanup.

    When environment is ready the script executes user-provided command without modification.
    """
    log_init()
    args = parse_args()
    if not args.availability_zone:
        args.availability_zone = AZ_PREFERENCE

    extra_tags = [isc_aws.tag_remove_after(minutes=args.remove_after_min)]

    # no retry EC2 client for faster AZ selection
    config = Config(
        retries={
            "max_attempts": 1,  # instead of default 3 which would use backoff time
            "mode": "standard",
        }
    )
    ec2 = boto3.resource("ec2", config=config)

    namer = random_uuid_namer(args.name_prefix, extra_tags)
    with isc_aws.EphemeralKeypairInFile(namer, args.skip_removal_on_exit) as keyp:
        for spot, azid in itertools.product([True, False], args.availability_zone):
            logging.info(
                "creating %s VMs in availability zone %s",
                (spot and "spot") or "on-demand",
                azid,
            )
            try:
                try_cluster(args, ec2, azid, namer, keyp, spot)
                break  # we are done, do no try other AZs
            except ClientError:
                logging.exception(
                    "failed to create %s instance in availability zone %s",
                    "spot" if spot else "on-demand",
                    azid,
                )
                # new UUID for the next try to avoid conflict on placement group etc.
                namer = random_uuid_namer(args.name_prefix, extra_tags)
            except subprocess.CalledProcessError:
                logging.error("test orchestration error: Ansible call has failed")
                sys.exit(1)
        else:
            logging.error(
                "test orchestration error: failed to create test VMs in all "
                "availability zones, tried set %s",
                args.availability_zone,
            )
            sys.exit(2)


if __name__ == "__main__":
    main()
