# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Python context managers for ephemeral SSH keys and VM clusters,
along with helpers for naming and tagging AWS objects.
"""

import datetime
import ipaddress
from typing import Any, Dict, List, Iterable
import logging
import tempfile
import uuid

import boto3
from botocore.exceptions import ClientError

EC2 = boto3.resource("ec2")
EC2_CLIENT = boto3.client("ec2")


class Namer:
    """Generator for object names and associated tags.
    At least 'isc:owner' tag needs to be provided."""

    def __init__(self, name: str, tags: List[Dict[str, str]]):
        self.name = name
        self.tags = tags

    def __str__(self):
        return self.name

    def sub_name(self, suffix: str):
        """Return new Namer object with prefix and tags inherited form this object."""
        return Namer(f"{self.name}{suffix}", tags=self.tags)

    def new_tags(self, restypes: Iterable[str]) -> List[Dict[str, Any]]:
        """New Boto3 tag specification for objects of given resource types (instance, volume ...).
        Returns list with a dict for each restype, with tag dict nested inside.
        Boto3 wants it that way."""
        tag_list = self.tags + [{"Key": "Name", "Value": self.name}]
        tag_spec = []
        for restype in restypes:
            tag_spec.append({"ResourceType": restype, "Tags": tag_list})
        return tag_spec


class EphemeralCluster:
    """Context manager to create ephemeral placement group with strategy "cluster".
    All instances are terminated when exiting the context."""

    def __init__(self, namer: Namer, skip_removal_on_exit=False):
        self.skip_removal_on_exit = skip_removal_on_exit
        self.namer = namer
        self.name = namer.name  # shortcut
        self.placegrp = EC2.PlacementGroup(self.name)

    def __enter__(self):
        logging.info("creating cluster placement group %s", self.name)
        EC2_CLIENT.create_placement_group(
            GroupName=self.name,
            Strategy="cluster",
            TagSpecifications=self.namer.new_tags(["placement-group"]),
        )
        logging.info("cluster placement group %s created", self.name)

        return self

    # pylint: disable=too-many-arguments
    def create_instances(
        self,
        suffix: str,
        create_params: Dict[str, Any],
        count=1,
        spot=True,
        ec2=EC2,
    ) -> List:
        """Start new instances in our placement group.
        Cluster name is used as prefix for instance name.
        Returns list of ec2.Instance objects from Boto3 factory.
        """
        restypes = ["instance", "network-interface", "volume"]
        if spot:
            restypes.append("spot-instances-request")
        inamer = self.namer.sub_name(suffix)
        instance_params = {
            "TagSpecifications": inamer.new_tags(restypes),
            "Placement": {
                "GroupName": self.name,
            },
        }
        if spot:
            spot_params = {
                "InstanceMarketOptions": {
                    "MarketType": "spot",
                },
            }
            instance_params.update(spot_params)
        instance_params.update(create_params)
        logging.info(
            "creating %d %s instances with name %s in placement group %s",
            count,
            "spot" if spot else "on-demand",
            inamer.name,
            self.name,
        )
        instances = ec2.create_instances(
            **instance_params, MinCount=count, MaxCount=count
        )
        assert len(instances) == count
        return instances

    def wait_for_startup(self) -> None:
        """Returns None when all instances are in running state, raises on timeout."""
        inst_ids = list(inst.id for inst in self.placegrp.instances.all())
        logging.info("waiting for instances %s to reach running state", inst_ids)
        waiter = EC2_CLIENT.get_waiter("instance_running")
        waiter.wait(InstanceIds=inst_ids)
        logging.info("all instances are running: %s", inst_ids)

    def __exit__(self, etype, value, traceback):
        logging.info("placement group %s cleanup phase started", self.name)
        inst_ids = list(inst.id for inst in self.placegrp.instances.all())
        if inst_ids:  # we actually did create something
            # beware - empty inst_ids would cause EC2 API to return unrelated instances!
            desc = EC2_CLIENT.describe_instances(InstanceIds=inst_ids)

            for reserv in desc["Reservations"]:
                for inst in reserv["Instances"]:
                    logging.info(
                        "instance %s: state %s",
                        inst["InstanceId"],
                        inst["State"]["Name"],
                    )
                    if inst["State"]["Code"] != 16:  # not running
                        logging.critical(
                            "instance %s: not running at the end of session: reason %s",
                            inst["InstanceId"],
                            (
                                inst.get("StateReason")
                                or inst.get("StateTransitionReason")
                            ),
                        )

        if self.skip_removal_on_exit:
            logging.info("NOT deleting ephemeral cluter %s now", self.name)
            return

        logging.info(
            "stopping and terminating all instances in placement group %s", self.name
        )

        if inst_ids:
            try:
                EC2_CLIENT.terminate_instances(InstanceIds=inst_ids)
                # wait takes too long, so we just call API to terminate
                # instances and proceed - if placement group deletion fails
                # (because some instances were not terminated in time) our
                # independent cleanup script will remove them eventually
                # logging.info('waiting for instances %s to be terminated', inst_ids)
                # waiter = EC2_CLIENT.get_waiter('instance_terminated')
                # waiter.wait(InstanceIds=inst_ids)
                # logging.info('instances %s terminated', inst_ids)
            except ClientError as ex:
                logging.error("error while terminating instances %s: %s", inst_ids, ex)

        logging.info("deleting placement group %s", self.name)
        try:
            self.placegrp.delete()
            logging.info("placement group %s deleted", self.name)
        except ClientError as ex:
            logging.warning("failed to delete placement group %s: %s", self.name, ex)


class EphemeralKeypair:
    """Context manager to create and destroy new SSH keypair via AWS API."""

    def __init__(self, namer: Namer, skip_removal_on_exit=False):
        self.skip_removal_on_exit = skip_removal_on_exit
        self.namer = namer
        self.keypair = None

    def __enter__(self):
        logging.info("creating ephemeral key pair %s", self.namer.name)
        self.keypair = EC2.create_key_pair(
            KeyName=self.namer.name, TagSpecifications=self.namer.new_tags(["key-pair"])
        )
        return self

    def __exit__(self, etype, value, traceback):
        if self.skip_removal_on_exit:
            logging.info("NOT deleting ephemeral key pair %s now", self.namer.name)
            return
        logging.info("deleting ephemeral key pair %s", self.namer.name)
        self.keypair.delete()
        self.keypair = None

    @property
    def instance_params(self) -> Dict[str, str]:
        """
        Parameter for EphemeralCluster.create_instances to start a new instance with this key.
        """
        assert self.keypair  # keypair was created and not deleted yet
        return {"KeyName": self.namer.name}


class EphemeralKeypairInFile(EphemeralKeypair):
    """Context manager to store ephemeral SSH key pair into file keyfile_path.
    File will be deleted and key removed from AWS on exit."""

    def __init__(self, namer, skip_removal_on_exit=False):
        super().__init__(namer, skip_removal_on_exit)
        self.keyfile = None

    def __enter__(self):
        super().__enter__()
        # pylint: disable=consider-using-with
        self.keyfile = tempfile.NamedTemporaryFile(
            mode="w", delete=not self.skip_removal_on_exit
        )
        self.keyfile.__enter__()
        self.keyfile.write(self.keypair.key_material)
        self.keyfile.flush()
        return self

    @property
    def keyfile_path(self) -> str:
        """Read only path to SSH private key."""
        if not self.keyfile:
            raise RuntimeError("use context manager before accessing the keyfile_path")
        return self.keyfile.name

    def __exit__(self, etype, value, traceback):
        if self.skip_removal_on_exit:
            logging.info("NOT deleting ephemeral key pair in %s", self.keyfile.name)
            return
        self.keyfile.__exit__(etype, value, traceback)
        super().__exit__(etype, value, traceback)


def tag_remove_after(**kwargs) -> Dict[str, str]:
    """Produce tag to schedule resource removal N seconds from now.
    Accepts same parameters as datetime.timedelta."""
    deadline = (
        datetime.timedelta(**kwargs) + datetime.datetime.now(tz=datetime.timezone.utc)
    ).isoformat()
    return {"Key": "isc:remove_after", "Value": deadline}


def get_instance_ipv6_prefix(instance_obj) -> ipaddress.IPv6Network:
    """Get the only IPv6 prefix delegated to the instance. Missing delegation is fatal."""
    assert len(instance_obj.network_interfaces) == 1, instance_obj.network_interfaces
    assert (
        len(instance_obj.network_interfaces[0].ipv6_prefixes) == 1
    ), instance_obj.network_interfaces[0].ipv6_prefixes
    return ipaddress.IPv6Network(
        instance_obj.network_interfaces[0].ipv6_prefixes[0]["Ipv6Prefix"]
    )


def add_instance_ipv6_addr(instance_obj) -> ipaddress.IPv6Address:
    """Get the only IPv6 prefix delegated to the instance. Missing delegation is fatal."""
    assert len(instance_obj.network_interfaces) == 1, instance_obj.network_interfaces
    eni_id = instance_obj.network_interfaces[0].id
    result = EC2_CLIENT.assign_ipv6_addresses(
        Ipv6AddressCount=1, NetworkInterfaceId=eni_id
    )
    return ipaddress.IPv6Address(result["AssignedIpv6Addresses"][0])


def main():
    """
    Only for testing.

    This is not a complete working example.
    """
    logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")

    randname = f"test-isc-aws-{uuid.uuid4()}"
    tags = [
        tag_remove_after(minutes=5),
        {"Key": "isc:provisioning", "Value": "isc_aws.py"},
    ]
    namer = Namer(randname, tags)

    with EphemeralKeypairInFile(namer) as keypair:
        print("SSH private key: ", keypair.keyfile_path)
        with EphemeralCluster(namer) as cluster:
            insts = cluster.create_instances(
                "-instance", create_params=keypair.instance_params
            )

            cluster.wait_for_startup()

            for instance in insts:
                print(instance.id, get_instance_ipv6_prefix(instance))
            print("do some useful work here")


if __name__ == "__main__":
    main()
