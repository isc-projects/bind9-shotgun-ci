# bind9-shotgun-ci

Run [DNS Shotgun](https://dns-shotgun.readthedocs.io) resolver benchmarks in AWS
directly from GitLab CI.

# Usage

Work in progress, please report problems to @nicki (@pspacek)

## Decide what you want to test

Configurable parameters:
- `SHOTGUN_TEST_VERSION`
  - list of versions to be tested
  - at least one value must be provided
  - accepts commit or branch or tag name
- `SHOTGUN_SCENARIO` - udp (default), tcp, dot, doh
- `SHOTGUN_TRAFFIC_MULTIPLIER` - simulated load - if unsure leave default value
  "10", which is roughly 94 k QPS
- `SHOTGUN_DURATION` - first 60 seconds (default) is most interesting because we
  always start with fresh instance and an empty cache
- `SHOTGUN_ROUNDS` - how many test rounds - three recommended (default) so we
  are not fooled by noise
- `SHOTGUN_FLAMEGRAPH` - whether flamegraph should be created, don't use if test
  is longer than 60 seconds otherwise runner will run out of space and the job
  will fail
- `SHOTGUN_SERVER_THREADS` - default 16 is also max; can be used to limit online
  CPUs to test performance with less cores
- `SHOTGUN_CI_IMAGE_TAG` - leave it as `latest` unless you know what you're
  doing

Parameters `SHOTGUN_TEST_VERSION`, `SHOTGUN_SCENARIO`,
`SHOTGUN_TRAFFIC_MULTIPLIER` accept either one string or list of strings in
Python syntax: `['main', 'v9.16.39', '1234567abcdef']`. If multiple parameters
contain list then Cartesian product of all provided values is tested.

Example 1: Determining maximum load for one version
- `SHOTGUN_TEST_VERSION` = `1234567abcdef`
- `SHOTGUN_SCENARIO` = `udp`
- `SHOTGUN_TRAFFIC_MULTIPLIER` = `[10, 12, 14]`
- `SHOTGUN_DURATION` = `60`
- `SHOTGUN_ROUNDS` = 3

Run specified version and fire at it over UDP "10 x base load", "12 x base
load", "14 x base load". Repeat three times for each load value. Produces 9 (3
load x 3 runs) charts with response rates. Good for finding maximum load by
determining when response load starts dropping.

Example 2: Comparing performance between versions
- `SHOTGUN_TEST_VERSION` = `['main', 'v9.16.39', '1234567abcdef']`
- `SHOTGUN_SCENARIO` = `udp`
- `SHOTGUN_TRAFFIC_MULTIPLIER` = `10`
- `SHOTGUN_DURATION` = `60`
- `SHOTGUN_ROUNDS` = 3

Run each version three times, and fire "10 x base load" at it over UDP (roughly
10 x 9.4 k QPS). Produces 9 (3 versions x 3 runs) charts with response rates.
Good for comparison between versions. Assumes the load (traffic multiplier) is
set to a value where at least one version is able to keep up, otherwise it would
be hard to interpret results.

Additional parameters may be configured for resource monitoring tool, cache size
etc. See the variables in `Run pipeline` for up-to-date complete list of options.

## Start test

Go to https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/pipelines/new, fill in the parameters and click on `Run pipeline`.

![run_pipeline](docs/run_pipeline.png)

## Getting results

Go to https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/pipelines and find
your new pipeline. The pipeline will dynamically create a "[child
pipeline](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)" where the
actual jobs and results will be. To access it, you can click on the performance
or downstream job.

![parent_pipeline](docs/parent_pipeline.png)

![child_pipeline](docs/child_pipeline.png)

Wait until all jobs are finished. Then download all artifacts from the
`postproc` job, it has all the charts and also (optionally) profiling flame
charts for each run.

The charts in the artifacts are various representations of the same data.
Depending on what you are trying to find out it might be beneficial to either
look at individual runs and study rcodes, or look at summary charts for all runs
without rcodes etc.


## Interpretation

See [video](https://ripe79.ripe.net/archives/video/198/) and
[slides](https://ripe79.ripe.net/presentations/45-benchmarking.pdf) with
explanation of basic principles.

Obviously DNS Shotgun does not provide information "why" something is happening,
that's left to you imagination.

Bear in mind that we are testing against the live Internet, so results are noisy
and can change over time. Do not compare "old" and "new" results, it's better to
retest then to chase ghosts of non-existing performance regressions.

Obviously you can also ask @nicki or @pspacek :-)


# Developer Documentation

## Overview

This repository glues together existing projects and provides the required
integration with AWS and GitLab CI.

The resolver benchmarks themselves are perfomed using [DNS
Shotgun](https://gitlab.nic.cz/knot/shotgun). This tool is responsible for
replaying the DNS traffic, capturing the replies and providing the raw results
in JSON format. It also contains scripts that can be used to plot charts from
these results.

Since testing with DNS Shotgun requires quite a complex setup between two
machines that act as a server and client, an existing Ansible automation for
benchmarking resolvers is used. The
[resolver-benchmarking](https://gitlab.nic.cz/knot/resolver-benchmarking/)
project can be used to setup two dedicated machines for the test, build the
required container images for the resolver, perform the test and collect the
results from the machines.

The benchmarking automation also supports monitoring of hardware resources on
the server in order to enable performance analysis. It utilizes the
[resource-monitor](https://gitlab.isc.org/isc-projects/resource-monitor) project
which captures raw data from the Linux system and then contains scripts to
process and plot this data.

The final pieces required for provisioning the machines from AWS and integrating
the whole workflow into GitLab CI are mostly available in this repository. Some
parts of the initial setup aren't automated are were manually set up in AWS for
our usage.

## GitLab CI/CD integration

### Runner

A runner that can spin up multiple VMs in AWS is required. We have a [dedicated
runner](https://gitlab.isc.org/isc-private/devops/-/issues/138) set up as a
docker runner. Runner VM needs couple [configuration
tricks](https://gitlab.isc.org/isc-private/devops/-/merge_requests/8) to get
IPv6 to work inside Docker container running on AWS VM.

Since the runner requires full access to EC2 service in AWS in order to
create and manage the VMs, it must be protected against abuse. It is configured
as a *specific runner* that is only assigned to this project. Only trusted users
can push code and/or trigger CI/CD in this repository.

The docker image we use for the runner is created from the
[images](https://gitlab.isc.org/isc-projects/images/-/tree/main/docker/shotgun-controller)
repo. The image contains all the necessities to operate the whole machinery -
mainly the projects mentioned above as well as this repository. The image itself
lives in AWS Elastic Container Registry (ECR). The tag `latest` is used for
production testing and other tags such as `staging` can be used to develop and
test changes to the workflow.

*Note: Since we require two VMs per job - a server and a client, GitLab's AWS
runner autoscaling can't be utilized for this purpose.*

### UI - Run Pipeline

The [Run
Pipeline](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/pipelines/new)
dialog is used as the interface and configuration for the workflow. Variables
defined in
[.gitlab-ci.yml](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/.gitlab-ci.yml#L1)
are customized by the user to configure which benchmarks should be performend.
Based on the variables, a single pipeline can generate many individual DNS
Shotgun runs -- repeated runs, different version, protocols, load etc.

### Pipeline and Child Pipeline

Once the pipeline is started, the input variables and processed and based on
them, the actual performance test(s) pipeline is generated. This is the
responsibility of the
[job-generator.py](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/job-generator.py).
Its output is a YAML file that will be used to create the child pipeline. In
theory, this is where other integration would be possible (e.g. generate only
a sequence of shell commands to be executed sequentially).

A job matrix is generated from the user-provided variables. For every distinct
version, a build job will be created which builds the docker image if needed and
pushes it into ECR to be re-used by subsequent jobs.

For every combination of test parameters, a separate performance test job is
generated. Each test job will require two machines - a server and a client.

### Performance Test Job

To perform this job, the
[shotgun_aws.py](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/aws/shotgun_aws.py)
script provisions two VMs in EC2 AWS. Once they boot up and are reachable over
SSH, it runs the resolver-benchmarking ansible playbook to which it passes the
test parameters via ansible variables file created by the previous job that
also created the child pipeline.

The performance test jobs are executed in parallel and are scaled by AWS, since
every job requests its own set of machines. However, there is a limit of how
many jobs the runner itself can take. While the runner doesn't do the
resource-intensive part of the test, its capacity is still limited, since it
serves as the control point for the jobs -- it runs the Ansible playbooks or the
postprocessing job(s).

### Postproc Job

The postprocessing job is a single place where all artifacts from all the runs
are collected. It also invokes the chart plotting scripts from shotgun and
resource-monitoring repo in a special way that allows drawing special charts
with multiple runs being pooled and plotted together. This is the responsibility
of the
[group_plot.py](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/group-plot.py)
script.

## AWS specialties

This section mentions some of the configuration that took place in AWS and any
other details relating to the use of AWS for this project.

### AMI and LaunchTemplate

The server and client VMs that are used for the performance tests use a base
image called AMI (Amazon Machine Image). We use a customized Arch Linux image
with pre-installed dependencies for our testing workflow. The
[bind9-shotgun-ami](https://gitlab.isc.org/isc-projects/images/-/tree/b41226b2d1c92c8993e1dec7222db417476b0395/aws/bind9-shotgun-ami)
is created with packer recipe. If you need to rebuild the AMI, follow the
instructions in the images repo, push the image to AWS and update the
LaunchTemplate in AWS web interface.

### LaunchTemplate

The LaunchTemplate is a recipe to spawn new VMs. Few important bits have been
configured here: the AMI template to use, instance type(s) and whether to use
spot or on-demand instances.

The template has to be configured to use on-demand instances, since that allows
provisioning both spot and on-demand instances programmatically from this
template. It is possible to switch the request to spot instance if the
LaunchTemplate wants on-demand instances, but it doesn't seem easily achievable
the other way around.

### Spot vs on-demand instances

Spot instances can be much cheaper, but they may not always be available or may
get interrupted. On-demand capacity is much more readily available is proved to
be more reliable during peak times.

To get the best of both worlds, our provisioning attempts to find spot instance
capacity in the configured availability zones and if it fails to do so, it falls
back to on-demand instances.

### Availability Zone (AZ)

Within our selected realm, `us-east-1`, the AZs are somewhat like distinct
data centers. As long as both the server and client are withing the same AZ, we
don't care in which AZ they actually are. We needed to configure a subnet for
each of these AZs and assign the right one when spawning VMs.

The AZs can also have different pricing for the spot instances. To optimize
costs, we have
[az_pricelist.py](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/aws/az_pricelist.py)
script that lists the AZs with cheapest spot instances first. This script is
executed by cron on the runner every hour and the resulting list is stored in
S3. This file is the retrieved and used when `shotgun_aws.py` provisions the VMs.

### ECR (container registry)

This is used as the container registry to store and retrieve the necessary
containers. It is a private registry and requires a login. This is done via the
[pre-test.yaml](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/aws/pre-test.yaml)
playbook.

Each repository has assigned its own lifecycle policy. It has been configured
via the AWS Web UI and it has been set to periodically removed older container
images.

### PCAP in S3

The input PCAP (shotgun pellets) is stored in smaller chunks that are meant to
be merged on the fly. The S3 objects are exposed to the local file system as
pipes via the
[s3_to_fifo.py](https://gitlab.isc.org/isc-projects/bind9-shotgun-ci/-/blob/7bbbb1390fe5f2c8746f682e2ef407a0820c8e9f/aws/s3_to_fifo.py)
script. Then, the cut-pcap.lua and merge-chunks.py Shotgun scripts are used to
merge the desired number of chunks together and cut them at the specified time.
The resulting PCAP file is stored locally and is thrown away along with the VM
when it is destroyed.

The reason for this gymnastics is that EBS storage which backs disks
in test VMs can have very slow **read** access to data cloned from
the disk snapshot.

This is [documented behavior](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-initialize.html)
of EBS - a consequence of cloning virtual disk sectors from snapshot only
on first read access.

Downloading data from S3 in bulk and writing it to VM disk is
**much** faster on first use because write performance is not
limited, and there is no fee for data transfers from S3 to EC2
within a single AZ.

### isc:remove_after

Since the performance test workflow may fail at any point after VMs are
provisioned, there has to be a mechanism to remove the expired VMs that may have
been left running by a problematic job.

To address this issue, all VMs are created with our special tag
`isc:remove_after` which contains a timestamp after which the VMs should be
cleaned up by an external script, cleanup_ephemeral.py, which is executed
periodically by CRON on the runner.
