#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Script to plot selected runs together as groups.
"""

import argparse
from collections.abc import Sequence
from contextlib import contextmanager
import itertools
import json
import logging
from pathlib import Path
import os
import shutil
import subprocess
from typing import Any

from jobgen import combinations
from jobgen import names
from jobgen.jobs import SHOTGUN_GIT_DIR, RESMON_GIT_DIR


def _call(cmd):
    logging.debug("%s", cmd)
    return subprocess.check_call(cmd)


class ResultGroup(Sequence):
    """Contains one or more jobs."""

    def __init__(self, specs, merged: frozenset[str]):
        self.specs = specs
        self.merged = merged
        self._validate()

    def _validate(self):
        """sanity check that groupping works"""
        assert len(self.specs)
        first = self.specs[0]
        check_fields = frozenset(first.keys()) - self.merged - names.IGNORE_KEYS
        for other in self.specs[1:]:
            for field in check_fields:
                if first[field] != other[field]:
                    raise ValueError(
                        f"jobs {first['job_name']} and {other['job_name']} can't belong "
                        "to the same results group when merging {self.merged}"
                    )

    @property
    def shotgun_jsons(self):
        """JSON filenames with DNS Shotgun data, see also shotgun_tmpdir()."""
        return [f"{spec['job_name']}.json" for spec in self.specs]

    @property
    def job_names(self):
        """Names of jobs within this group."""
        return [f"{spec['job_name']}" for spec in self.specs]

    def __getitem__(self, index):
        return self.specs[index]

    def __len__(self):
        return len(self.specs)


def supergroup_name_template(groups):
    """
    Construct name template which disambiguates individual groups
    in given supergroup.
    """
    specs = []
    merged = None
    for group in groups:
        specs.extend(group.specs)
        if merged is None:
            merged = group.merged
        else:
            assert (
                merged == group.merged
            ), "supergroup must use exactly same merge keys in all groups"
    assert merged is not None

    fields = combinations.group_keys(
        specs, ignored_keys=merged.union(names.IGNORE_KEYS)
    )
    return names.construct_jobname(fields)


def shotgun_results_available(spec):
    """Verify that DNS Shotgun results from the job defined by spec are available."""
    json_dir = Path(spec["job_name"], "results-shotgun", "data").resolve()
    if not json_dir.is_dir():
        logging.debug("path %s is not a directory, missing results?", json_dir)
        return False
    json_file = list(json_dir.glob("*.json"))
    if not json_file:
        logging.debug("no JSON found in %s", json_dir)
        return False
    return True


def resmon_data_available(spec):
    """Verify resource monitor data from the job defined by spec are available."""
    captured = Path(spec["job_name"], "resmon-captured.json").resolve()
    if not captured.exists():
        logging.debug("missing resmon file: %s", captured)
        return False
    # shotgun data is needed by resource-monitor/plot.py to determine timestamp
    if not shotgun_results_available(spec):
        return False
    return True


def get_result_groups(specs, merge: frozenset[str], validate_fn=None):
    """Group together results across the specified merge fields."""
    fields = combinations.group_keys(specs, merge.union(names.IGNORE_KEYS))
    ordered = list(sorted(fields))

    groups = {}  # type: dict[tuple, list[dict]]
    for spec in specs:
        if validate_fn is not None and not validate_fn(spec):
            logging.warning("skipping %s: missing data", spec["job_name"])
            continue
        group_id = tuple(spec[field] for field in ordered)
        group = groups.setdefault(group_id, [])
        group.append(spec)
        logging.debug("%s: %s", group_id, spec)
    result_groups = []
    for group_specs in groups.values():
        result_groups.append(ResultGroup(group_specs, merge))
    return result_groups


@contextmanager
def shotgun_tmpdir(specs):
    """Prepares a temporary shotgun directory in cwd."""
    tmpdir = Path("tmp-shotgun").resolve()
    if tmpdir.is_dir():
        raise RuntimeError(f"{tmpdir} already exists")
    os.mkdir(tmpdir)
    logging.debug("created temporary directory %s", tmpdir)

    # find shotgun results and symlink them to tmpdir (for proper names in charts)
    for spec in specs:
        if not shotgun_results_available(spec):
            raise RuntimeError(f"shotgun results for {spec['job_name']} unavailable!")
        json_dir = Path(spec["job_name"], "results-shotgun", "data")
        json_file = list(json_dir.glob("*.json"))
        if len(json_file) > 1:
            raise NotImplementedError(
                "multiple JSONs for single shotgun run aren't supported yet"
            )
        json_file = json_file[0]
        link_file = Path(tmpdir, f'{spec["job_name"]}.json')
        link_file.symlink_to(json_file.resolve())
        logging.debug("created file link %s", link_file)

    try:
        yield tmpdir
    finally:
        shutil.rmtree(tmpdir)
        logging.debug("deleted temporary directory %s", tmpdir)


def chart_name(separate_charts, groups):
    """Generate chart name based on used CLI arguments and input data."""
    if not separate_charts:
        name = "all"
        groupped = not all(len(group) == 1 for group in groups)
        if groupped:
            name += "-groups"
        return name

    subgroup_template = names.construct_jobname(separate_charts)
    return subgroup_template.substitute(groups[0].specs[0])


def prepare_chart_path(name, root="."):
    """Prepare chart name, directory and return output path."""
    dirname = Path(root, f"_charts-{name}")
    if dirname.is_dir():
        return dirname / name
    logging.info("creating chart output directory: %s", dirname)
    os.mkdir(dirname)
    return dirname / name


def plot_latency_chart(  # pylint: disable=too-many-arguments
    path, groups, img_format, linestyles, start_time=0, end_time=None
):
    """Plot a single latency chart."""
    end_time_desc = end_time or "end"
    filepath = f"{path.resolve()}-latency-since_{start_time}-until_{end_time_desc}.{img_format}"
    logging.info("plotting latency chart: %s", filepath)
    cmd = [
        f"{SHOTGUN_GIT_DIR}/tools/plot-latency.py",
        "-t",
        f"Latency, {path.name}, since test time {start_time} until {end_time_desc}",
        "-o",
        filepath,
        "--since",
        str(start_time),
    ]
    if end_time:
        cmd.extend(["--until", str(end_time)])

    for name_re, style in linestyles.items():
        cmd.extend(["--linestyle", name_re, style])

    groupped = not all(len(group) == 1 for group in groups)
    if not groupped:
        cmd.extend(itertools.chain(*[group.shotgun_jsons for group in groups]))
    else:
        template = supergroup_name_template(groups)
        for group in groups:
            cmd.extend(["-g", template.substitute(group.specs[0])])
            cmd.extend(group.shotgun_jsons)
    _call(cmd)


def _determine_time_boundaries(args, specs):
    # check all tests have the same length
    assert len(specs) >= 1
    pcap_lengths = [int(spec["pcap_length_s"]) for spec in specs]
    length = pcap_lengths[0]
    if not all(pcap_length == length for pcap_length in pcap_lengths):
        raise NotImplementedError("all tests must have the same length")

    if (args.since and args.since_rel) or (args.until and args.until_rel):
        raise RuntimeError(
            "can't use both absolute and relative boundaries for --since or --until"
        )

    since = 0
    if args.since:
        since = args.since
    elif args.since_rel:
        since = int(length * args.since_rel)

    until = length
    if args.until:
        until = args.until
    elif args.until_rel:
        until = int(length * args.until_rel)

    if since > until:
        raise RuntimeError("--since happens after --until")
    if since < 0 or until > length:
        raise RuntimeError("invalid --since/--until")

    return since, until


def _get_specs(charts):
    specs = []
    for group in itertools.chain(*charts.values()):
        specs.extend(group.specs)
    return specs


def latency(args, charts, linestyles):
    """Subcommand to plot latency chart(s)."""
    logging.debug("subcommand: latency")
    specs = _get_specs(charts)
    start, end = _determine_time_boundaries(args, specs)
    with shotgun_tmpdir(specs) as tmpdir:
        os.chdir(tmpdir)
        for supergroup_key, groups in charts.items():
            path = prepare_chart_path(supergroup_key, "..")
            plot_latency_chart(path, groups, args.img_format, linestyles, start, end)


def plot_connections_chart(path, groups, img_format):
    """Plot connections statistics chart."""
    filepath = f"{path.resolve()}-connections.{img_format}"
    logging.info("plotting connections chart: %s", filepath)
    cmd = [
        f"{SHOTGUN_GIT_DIR}/tools/plot-connections.py",
        "-t",
        f"Connections, {path.name}",
        "-o",
        filepath,
    ]
    cmd.extend(itertools.chain(*[group.shotgun_jsons for group in groups]))
    _call(cmd)


def connections(args, charts, _linestyles):
    """Subcommand to plot connection statistics chart(s)."""
    logging.debug("subcommand: connections")
    specs = _get_specs(charts)
    with shotgun_tmpdir(specs) as tmpdir:
        os.chdir(tmpdir)
        for supergroup_key, groups in charts.items():
            path = prepare_chart_path(supergroup_key, "..")
            shotgun_jsons = list(
                itertools.chain(*[group.shotgun_jsons for group in groups])
            )
            handshakes = set()
            for json_name in shotgun_jsons:
                stats = json.loads(Path(json_name).read_text(encoding="utf-8"))
                handshakes.add(
                    (
                        stats["stats_sum"]["conn_handshakes"],
                        stats["stats_sum"]["conn_handshakes_failed"],
                    )
                )
            if len(handshakes) <= 1 and handshakes.pop() == (0, 0):
                logging.info(
                    "skipping connection plot: %s (0 handshakes)", supergroup_key
                )
                continue
            plot_connections_chart(path, groups, args.img_format)


def plot_response_rate_charts(path, groups, img_format):
    """Plot response rate charts."""
    cmd_base = [f"{SHOTGUN_GIT_DIR}/tools/plot-response-rate.py"]
    title_prefix = f"Response rate, {path.name}"

    shotgun_jsons = list(itertools.chain(*[group.shotgun_jsons for group in groups]))

    filepath = f"{path.resolve()}-response_rate.{img_format}"
    logging.info("plotting response rates: %s", filepath)
    _call(
        cmd_base
        + [
            "-t",
            f"{title_prefix}, all RCODEs combined",
            "-o",
            filepath,
        ]
        + shotgun_jsons
    )

    filepath = f"{path.resolve()}-response_rate-rcodes.{img_format}"
    logging.info("plotting response rates with RCODEs: %s", filepath)
    _call(
        cmd_base
        + [
            "-t",
            f"{title_prefix}, only RCODEs with overall frequency > 1 %",
            "-o",
            filepath,
            "--skip-total",
            "--rcodes-above-pct",
            "1",
        ]
        + shotgun_jsons
    )

    filepath = f"{path.resolve()}-response_rate-noerror-nxdomain.{img_format}"
    logging.info("plotting response rates for NOERROR + NXDOMAIN: %s", filepath)
    _call(
        cmd_base
        + [
            "-t",
            f"{title_prefix}, sum of NOERROR + NXDOMAIN answers",
            "-o",
            filepath,
            "--skip-total",
            "--sum-rcodes",
            "NOERROR",
            "NXDOMAIN",
            "--",
        ]
        + shotgun_jsons
    )


def response_rates(args, charts, _linestyles):
    """Subcommand to plot response rates chart(s)."""
    logging.debug("subcommand: response-rates")
    specs = _get_specs(charts)
    with shotgun_tmpdir(specs) as tmpdir:
        os.chdir(tmpdir)
        for supergroup_key, groups in charts.items():
            path = prepare_chart_path(supergroup_key, "..")
            plot_response_rate_charts(path, groups, args.img_format)


def _prepare_resmon_parsed(specs):
    for spec in specs:
        if not resmon_data_available(spec):
            raise RuntimeError(f"resmon data for {spec['job_name']} unavailable!")
        dir_path = Path(spec["job_name"])
        captured = Path(dir_path, "resmon-captured.json").resolve()
        parsed = Path(dir_path, "resmon-parsed").resolve()
        if parsed.exists():
            logging.debug("skipping parsing resmon, already exists: %s", parsed)
            continue
        if not captured.exists():
            raise RuntimeError(f"missing resmon file: {captured}")
        logging.info("preparing resmon-parsed dir: %s", parsed)
        _call([f"{RESMON_GIT_DIR}/parse.py", "-i", captured, "-o", parsed])


def plot_resmon_charts(path, groups, img_format, linestyles):
    """Plot resource usage charts."""

    prefix = f"{path.resolve()}-resmon."
    logging.info("plotting resource monitor charts: %s", prefix)
    cmd = [
        f"{RESMON_GIT_DIR}/plot.py",
        "--prefix",
        prefix,
        "--img-format",
        img_format,
    ]

    for name_re, style in linestyles.items():
        cmd.extend(["--linestyle", name_re, style])

    groupped = not all(len(group) == 1 for group in groups)
    if not groupped:
        cmd.extend(itertools.chain(*[group.job_names for group in groups]))
    else:
        template = supergroup_name_template(groups)
        for group in groups:
            cmd.extend(["-g", template.substitute(group.specs[0])])
            cmd.extend(group.job_names)
    _call(cmd)


def resmon(args, charts, linestyles):
    """Subcommand to plot resource monitoring chart(s)."""
    logging.debug("subcommand: resmon")
    specs = _get_specs(charts)
    _prepare_resmon_parsed(specs)
    for supergroup_key, groups in charts.items():
        path = prepare_chart_path(supergroup_key)
        plot_resmon_charts(path, groups, args.img_format, linestyles)


def _add_latency_parser(subparsers):
    parser = subparsers.add_parser("latency", help="latency histogram(s)")
    parser.set_defaults(func=latency, validate_fn=shotgun_results_available)
    parser.add_argument(
        "--since-rel",
        type=float,
        help="use data since this part of the test (relative, range 0-1)",
    )
    parser.add_argument(
        "--until-rel",
        type=float,
        help="use data until this part of the test (relative, range 0-1)",
    )
    parser.add_argument(
        "--since",
        type=int,
        help="use data since this part of the test (absolute, in seconds)",
    )
    parser.add_argument(
        "--until",
        type=int,
        help="use data until this part of the test (absolute, in seconds)",
    )


def _add_resmon_parser(subparsers):
    parser = subparsers.add_parser("resmon", help="resource monitor charts")
    parser.set_defaults(func=resmon, validate_fn=resmon_data_available)


def _add_connections_parser(subparsers):
    parser = subparsers.add_parser("connections", help="connection statistics chart(s)")
    parser.set_defaults(func=connections, validate_fn=shotgun_results_available)


def _add_response_rates_parser(subparsers):
    parser = subparsers.add_parser("response-rates", help="response rates chart(s)")
    parser.set_defaults(func=response_rates, validate_fn=shotgun_results_available)


def _parse_args() -> tuple[argparse.Namespace, list[dict[str, Any]]]:
    parser = argparse.ArgumentParser(
        description="Group results from multiple runs and plot selected charts"
    )
    parser.add_argument(
        "--merge",
        action="append",
        default=[],
        help="merge results with the specified field into a single group (typically just 'run')",
    )
    parser.add_argument(
        "--separate-charts",
        action="append",
        default=[],
        help="plot a separate chart for unique values of this field(s), typically not used or "
        "'version' if you have large test matrix with multiple versions",
    )
    parser.add_argument(
        "--img-format", type=str, default="png", help="output image format"
    )
    parser.add_argument(
        "--combinations-file",
        type=argparse.FileType("r"),
        default="combinations.json",
        help="JSON file with test definitions",
    )
    subparsers = parser.add_subparsers(help="chart(s) to plot", required=True)
    _add_latency_parser(subparsers)
    _add_resmon_parser(subparsers)
    _add_connections_parser(subparsers)
    _add_response_rates_parser(subparsers)

    args = parser.parse_args()
    args.merge = frozenset(names.human_to_internal(args.merge))
    args.separate_charts = frozenset(names.human_to_internal(args.separate_charts))

    if (
        args.merge
        and args.separate_charts
        and not set(args.merge).isdisjoint(args.separate_charts)
    ):
        parser.error(
            "one field can't be specified for both --merge and --separate-charts"
        )

    specs = json.load(args.combinations_file)

    all_fields = set(frozenset(spec.keys()) for spec in specs)
    if len(all_fields) != 1:
        parser.error(
            f"Unsupported input: job specs have non-uniform list of fields! Got: {all_fields}"
        )

    valid_fields = all_fields.pop()
    for argument in ["merge", "separate_charts"]:
        requested = getattr(args, argument)
        unknown = requested.difference(valid_fields)
        if unknown:
            parser.error(f"'{argument}' field list contains unknown fields: {unknown}")

    return args, specs


LINE_STYLES = ["solid", "dashed", "dashdot", "dotted"]


def version_linestyles(specs):
    """Get version regexes and their corresponding line styles."""
    linestyles = {}
    i = 0
    for spec in specs:
        version = spec["version_safename"]
        if version not in linestyles:
            linestyles[version] = LINE_STYLES[i % len(LINE_STYLES)]
            i += 1
    return {f"^{version}": style for version, style in linestyles.items()}


def main():
    """Script entrypoint"""
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)-15s %(levelname)8s %(message)s"
    )
    args, specs = _parse_args()

    linestyles = version_linestyles(specs)
    groups = get_result_groups(specs, args.merge, validate_fn=args.validate_fn)
    logging.info("number of result groups: %d", len(groups))
    charts = {}

    if args.separate_charts:
        supergroup_template = names.construct_jobname(args.separate_charts)
        for g in groups:
            supergroup_key = supergroup_template.substitute(g.specs[0])
            charts.setdefault(supergroup_key, []).append(g)
    else:
        if not args.merge:
            name = "all"
        else:
            name = "all-groups"
        charts[name] = groups
    logging.info("%d chart(s) will be generated", len(charts))
    logging.debug("charts: %r", charts)

    args.func(args, charts, linestyles)  # call function based on chosen subcommand


if __name__ == "__main__":
    main()
