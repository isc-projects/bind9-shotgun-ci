#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Script to generate DNS Shotgun jobs and necessary data.
"""

import argparse
import ast
from collections import ChainMap
import copy
import json
import functools
import os
from pathlib import Path
import re
from typing import List

import gitlab  # pylint: disable=import-error

from jobgen import combinations, names
from jobgen.ansiblevars import Av
from jobgen.combinations import Template
from jobgen.jobs import AWSAnsibleBuildJob, AWSAnsibleTestJob, PostProcJob
from jobgen.generators import GitlabCIGenerator


BIND9_PROJECT_ID = 1
BIND9S_PROJECT_ID = 9


def literal_or_string(instr):
    """convert identifiers to strings, i.e. xxx -> 'xxx'"""
    if instr is None:
        raise ValueError("refusing to accept value None")
    try:
        val = ast.literal_eval(instr)
        if not isinstance(val, (str, list, int, float)):
            raise NotImplementedError("unsupported input expression")
        return val
    except (ValueError, SyntaxError):
        return str(instr)


def list_wrap(value):
    """
    Wraps atomic value in new single-item list. Return lists unmodified.
    """
    if not isinstance(value, list):
        return [value]
    return value


def env_val(env_var, default=None):
    """Obtain value from environment variable or return default"""
    try:
        return literal_or_string(os.environ.get(env_var, default))
    except ValueError as ex:
        raise ValueError(f"value not acceptable for env var {env_var}") from ex


def list_one_to_n(max_n):
    """User friendly range variant for 1-based arrays"""
    return list(range(1, max_n + 1))


@functools.cache
def generate_image_tag(resolver: str, version: str) -> str:
    """Create a container image tag for BIND to be able to find it in AWS ECR."""
    # HACK: This is basically a duplicated code from the resolver-benchmarking playbook:
    # https://gitlab.nic.cz/knot/resolver-benchmarking/-/blob/23fcb5a53b0fa20d0971f93660d9e508774cf5a4/roles/bind/tasks/main.yaml#L59
    # It's not pretty but we need it here in order to determine whether the image is already
    # available from ECR without running the Ansible playbook (which would require spawning
    # a VM...).
    if resolver != "bind":
        return ""

    def image_tag_from_ref_name(proj, ref_name):
        try:
            branch = proj.branches.get(ref_name)
        except gitlab.exceptions.GitlabGetError:
            pass
        else:
            # for branch names, always return commit SHA
            return branch.commit["id"]
        try:
            commit = proj.commits.get(ref_name)
        except gitlab.exceptions.GitlabGetError:
            return None
        # if we used shortened commit ID, return the full commit SHA
        if re.search(f"^{ref_name}", commit.id) is not None:
            return commit.id
        # only remaining option is for ref_name to be a tag - use its name
        return ref_name

    gl = gitlab.Gitlab(
        "https://gitlab.isc.org", private_token=env_val("BIND_TEAM_API_TOKEN", "")
    )
    bind9_proj = gl.projects.get(BIND9_PROJECT_ID)
    image_tag = image_tag_from_ref_name(bind9_proj, version)
    if image_tag is not None:
        return image_tag
    try:
        bind9s_proj = gl.projects.get(BIND9S_PROJECT_ID)
    except gitlab.exceptions.GitlabGetError:
        raise RuntimeError(
            "ref not found in public repo and security repo unavailable",
            version,
        ) from None
    image_tag = image_tag_from_ref_name(bind9s_proj, version)
    if image_tag is not None:
        return image_tag
    raise RuntimeError("ref not found in public or security repo", version)


def resmon_interval_auto(runtime_s) -> float:
    """
    Calculate appropriate resource-monitor stat collection interval relative
    to test runtime. Provides between 300-3000 samples.
    """
    if runtime_s < 5 * 60:
        return 0.1
    if runtime_s < 50 * 60:
        return 1
    if runtime_s < 8 * 3600:
        return 10
    if runtime_s < 2 * 24 * 3600:
        return 60
    return 300


GITLABCI_TEST_BASE = {
    # == CONSTANTS for Gitlab CI ==
    # cleanup on test VMs - not needed
    Av("auto_cleanup"): False,
    Av("auto_tuning"): True,
    Av("tuning_sysctl_file"): "",
    Av("collect_logs"): True,
    Av("resource_monitoring"): True,
    Av("resource_monitor_version"): "d5d041d66c69df8cd1963924b0584dcab415f4f2",
    Av("shotgun_registry"): "766250944489.dkr.ecr.us-east-1.amazonaws.com/shotgun",
    Av("shotgun_version"): "v20240219",
    Av(
        "flamegraph_registry"
    ): "766250944489.dkr.ecr.us-east-1.amazonaws.com/flamegraph",
    Av("flamegraph_git_sha"): "1b1c6deede9c33c5134c920bdb7a44cc5528e9a7",
    Av("bind_named_append_args"): env_val("BIND_NAMED_APPEND_ARGS", default=""),
    Av("bind_extra_conf"): env_val("BIND_EXTRA_CONF", default=""),
    Av("bind_extra_opt"): env_val("BIND_EXTRA_OPT", default=""),
    Av("bind_max_cache_size"): list(
        map(
            lambda mb: int(mb) * 1024 * 1024,
            list_wrap(env_val("CACHE_SIZE_MB", default=30 * 1024)),
        )
    ),
    "CACHE_SIZE_MB": (lambda test: int(test["bind_max_cache_size"] / 1024 / 1024)),
    Av("bind_recursive_clients"): env_val("BIND_RECURSIVE_CLIENTS", default=10000),
    "RESMON_INTERVAL": env_val("RESMON_INTERVAL", default="auto"),
    "RESMON_DATA": env_val("RESMON_DATA", default="cpumem"),
    Av("resmon_interval"): (
        lambda test: (
            resmon_interval_auto(test["pcap_length_s"])
            if test["RESMON_INTERVAL"] == "auto"
            else int(test["RESMON_INTERVAL"])
        )
    ),
    Av("resmon_allstats"): lambda test: test["RESMON_DATA"] != "cpumem",
    Av("resmon_bindstats"): lambda test: test["RESMON_DATA"] == "bindstats",
    # terminate and delete all test VMs after this many minutes + extra 30 minutes for test setup
    "CLEANUP_AFTER_SEC": (lambda test: test.get("pcap_length_s", 900) + 1800),
    "CLEANUP_AFTER_MIN": (
        lambda test: int(combinations.eval_val(test["CLEANUP_AFTER_SEC"], test) / 60)
    ),
    # BIND build optimization
    Av("bind_base_tag"): "build-base-image",
    Av("bind_base_meson_tag"): "build-base-meson-image",
    Av(
        "bind_registry"
    ): "766250944489.dkr.ecr.us-east-1.amazonaws.com/bind-shotgun-cache",
    # compatibility with BIND v9.11
    Av("doh_port"): (
        lambda test: 0 if test["shotgun_scenario"] in {"udp", "tcp"} else 443
    ),
    Av("dot_port"): (
        lambda test: 0 if test["shotgun_scenario"] in {"udp", "tcp"} else 853
    ),
    Av("perf_record"): env_val("SHOTGUN_FLAMEGRAPH", "0"),
    # for other resolver names see
    # https://gitlab.nic.cz/knot/resolver-benchmarking/-/tree/master/roles
    Av("resolver"): env_val("SHOTGUN_RESOLVER", "bind"),
    "version_safename": lambda spec: re.sub(
        "[^a-zA-Z0-9._-]", "", spec["version"].replace("/", "-")
    ),
    # directory with shotgun pellets to create the test PCAP
    "dataset": env_val("SHOTGUN_DATASET", "telco-eu-2022-02"),
    Av("pcap_s3_prefix"): Template("bind9-resolver-benchmark/$dataset/"),
    # file auto-generated by aws/pre-test.yaml
    Av("shotgun_pcap"): "/pcap/thisrun.pcap",
    # path at the end must be directory with PCAPs - i.e. defines data set for test
    Av("pcap_num_chunks"): Template("${SHOTGUN_TRAFFIC_MULTIPLIER}"),
    Av("srv_threads"): 16,
    # user-modifiable, common definition for all runs
    # for other supported values see
    # https://gitlab.nic.cz/knot/resolver-benchmarking/-/tree/master/roles/shotgun/files
    Av("shotgun_scenario"): env_val("SHOTGUN_SCENARIO", "udp"),
    Av("pcap_length_s"): int(env_val("SHOTGUN_DURATION", 60)),  # seconds
    "SHOTGUN_TRAFFIC_MULTIPLIER": env_val("SHOTGUN_TRAFFIC_MULTIPLIER", 10),
    "run": 1,  # default to 1 run for each variant
    Av("srv_threads"): env_val("SHOTGUN_SERVER_THREADS", 16),
    Av("dnssec"): env_val("SHOTGUN_DNSSEC", "1"),
    # propagate SHOTGUN_EVAL_THRESHOLD_* for postproc results auto-evaluation
    Av("SHOTGUN_EVAL_THRESHOLD_CPU_MIN"): env_val("SHOTGUN_EVAL_THRESHOLD_CPU_MIN", ""),
    Av("SHOTGUN_EVAL_THRESHOLD_CPU_MAX"): env_val("SHOTGUN_EVAL_THRESHOLD_CPU_MAX", ""),
    Av("SHOTGUN_EVAL_THRESHOLD_MEMORY_MIN"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_MEMORY_MIN", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_MEMORY_MAX"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_MEMORY_MAX", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_RCODE_MIN"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_RCODE_MIN", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_RCODE_MAX"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_RCODE_MAX", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_MIN"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_MIN", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_MAX"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_MAX", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_DRIFT_MIN"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_DRIFT_MIN", ""
    ),
    Av("SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_DRIFT_MAX"): env_val(
        "SHOTGUN_EVAL_THRESHOLD_LATENCY_PCTL_DRIFT_MAX", ""
    ),
}


def finish_jobspec(spec, group_keys):
    """Generate fields which depend on all the rest, e.g. name"""
    update = {}
    update["image_tag"] = generate_image_tag(spec["resolver"], spec["version"])
    update["job_name"] = names.construct_jobname(group_keys).eval_substitute(spec)
    # path on controller (the machine executing Ansible playbook)
    update[Av("results_directory")] = f"{os.getcwd()}/{update['job_name']}"

    assert set(str(key) for key in update).issubset(
        names.IGNORE_KEYS
    ), "auto-generated keys must be ignored"
    for key in update:
        assert key not in spec, f"unexpected attempt to override {key}"
    return update


def generate_gitlabci(azs: List[str]) -> str:
    """Create GitLab CI YAML (child pipeline) config for the testing workflow."""
    test_spec = ChainMap({}, GITLABCI_TEST_BASE)  # type: ignore
    test_spec[Av("version")] = env_val("SHOTGUN_TEST_VERSION")
    test_spec["run"] = list_one_to_n(int(env_val("SHOTGUN_ROUNDS", 1)))

    job_specs = list(combinations.expand_spec(test_spec))
    group_keys = combinations.group_keys(job_specs, ignored_keys=names.IGNORE_KEYS)

    for spec in job_specs:
        spec.update(finish_jobspec(spec, group_keys))

    build_jobs = {}
    for job_spec in job_specs:
        if not "image_tag" in job_spec:
            continue
        assert job_spec["resolver"] == "bind"  # not implemented for anything else yet
        build_id = (job_spec["resolver"], job_spec["image_tag"])
        build_id_name = "-".join(build_id)
        if build_id not in build_jobs:
            build_spec = copy.deepcopy(job_spec)
            build_spec["job_name"] = build_id_name
            build_spec["results_directory"] = str(Path(build_id_name).resolve())
            build_jobs[build_id] = AWSAnsibleBuildJob(build_spec, azs)
        # must be listed in names.IGNORE_KEYS to enable proper grouping
        job_spec["needs"] = build_id_name

    combinations.to_json_file(job_specs, "combinations.json")

    gen = GitlabCIGenerator()
    for build_job in build_jobs.values():
        gen.build_jobs.append(build_job)

    # test length == 0 means "build-only" pipeline
    if any(job_spec["pcap_length_s"] > 0 for job_spec in job_specs):
        for job_spec in job_specs:
            gen.test_jobs.append(AWSAnsibleTestJob(job_spec, azs))

        sample = None
        baseline = None
        postproc_spec = copy.deepcopy(job_specs[0])  # use first job_spec for env vars
        postproc_spec["job_name"] = "postproc"
        if len(gen.test_jobs) == 2:  # run auto-eval if there's just two jobs
            sample = gen.test_jobs[0].spec["job_name"]
            baseline = gen.test_jobs[1].spec["job_name"]
        gen.postproc_jobs.append(
            PostProcJob(postproc_spec, sample=sample, baseline=baseline)
        )

    return gen.generate()


def main():
    """Script entrypoint"""
    parser = argparse.ArgumentParser(
        description="Generate DNS Shotgun jobs and necessary data"
    )
    parser.add_argument(
        "-g",
        "--generator",
        choices=["gitlabci"],
        default="gitlabci",
        help="select what kind of output to generate",
    )
    parser.add_argument(
        "--availability-zones-file",
        type=argparse.FileType("r"),
        default=None,
        help="path to JSON file with list of AWS availability zones sorted by preference",
    )
    args = parser.parse_args()

    azs = []
    if args.availability_zones_file:
        azs = json.load(args.availability_zones_file)

    if args.generator == "gitlabci":
        print(generate_gitlabci(azs))


if __name__ == "__main__":
    main()
