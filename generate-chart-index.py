#!/usr/bin/python3
# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.
"""
Script to generate DNS Shotgun jobs and necessary data.
"""

import json
import html as htmlesc
import os
import re

HTML_BEGIN = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DNS Shotgun Charts</title>
  <style>
    h1 {
      text-align: center;
      margin-top: 30px;
    }
    h2 {
      text-align: center;
    }
    h3 {
        margin-top: 20px;
        margin-bottom: 10px;
    }
    table {
      border-collapse: collapse;
      width: 100%;
      max-width: 800px; /* adjust this value to set the maximum table width */
    }
    th, td {
      border: 1px solid #ddd;
      padding: 8px;
      text-align: left;
      width: 25%;
    }
    td.num {
      text-align: right;
    }
    td.highlight {
      font-weight: bold;
    }
    td.failed {
      background-color: #ffe6e6; /* light red background */
      color: #ff0000; /* red text */
    }
    td.passed {
      background-color: #c6efce; /* light green background */
    }
    table tr:nth-child(odd) {
      background-color: #f2f2f2; /* light gray background */
    }
    table tr:nth-child(even) {
      background-color: #ffffff; /* white background */
    }
    .chart {
      width: 98%;
      margin: 15px auto;
      display: block;
      border: 1px solid #ddd;
      padding: 5px;
      box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.1);
    }
  </style>
</head>
<body>
"""

HTML_END = """
</body>
</html>
"""

CHART_DIRS = ["_charts-all-groups", "_charts-all"]
CATEGORIES = {
    "Latency": re.compile(r".*latency.*\.png"),
    "Memory": re.compile(r".*resmon.memory.current.*\.png"),
    "CPU": re.compile(r".*resmon.cpu.usage_percent.*\.png"),
    "Response codes": re.compile(r".*response_rate-rcodes.*\.png"),
}


def eval_from_json() -> str:
    """Format JSON evaluation results into HTML."""
    html = ""
    try:
        with open("evaluation.json", "r", encoding="utf-8") as f:
            data = json.load(f)
    except FileNotFoundError:
        return html

    table_start = f"""<table>
    <tr>
      <th>Comparison</th>
      <th>{data['baseline']['name']} (baseline)</th>
      <th>{data['sample']['name']} (sample)</th>
      <th>Diff</th>
    </tr>
"""

    html += "  <h1>Result auto-evaluation</h1>\n"
    for result in data["results"]:
        html += f"  <h3>{result['icon']} {result['label']}</h3>\n"
        html += table_start
        for comparison in result["results"]:
            html += "    <tr>\n"
            html += f'      <td>{comparison["name"]}</td>\n'
            html += f'      <td class="num">{comparison["baseline_str"]}</td>\n'
            html += f'      <td class="num">{comparison["sample_str"]}</td>\n'
            html += f'      <td class="num highlight {"passed" if comparison["passed"] else "failed"}">{comparison["diff_str"]}</td>\n'
            html += "    </tr>\n"
        html += "  </table>\n"
    return html


def main():  # pylint: disable=missing-function-docstring
    job_url = os.getenv("CI_JOB_URL", default="")
    title = os.getenv("PIPELINE_NAME", default="")
    html = ""
    if title:
        html += f'<a href="{htmlesc.escape(job_url)}">{htmlesc.escape(title)}</a>\n'

    html += eval_from_json()
    for chart_dir in CHART_DIRS:
        if not os.path.exists(chart_dir):
            continue
        html += f"<h1>{chart_dir}</h1>\n"
        charts = sorted(os.listdir(chart_dir))
        for category, regex_filter in CATEGORIES.items():
            selected_charts = [chart for chart in charts if regex_filter.match(chart)]
            if not selected_charts:
                continue
            html += f"<h2>{category}</h2>\n"
            for chart in selected_charts:
                img = f'<img class="chart" src="{chart_dir}/{chart}" alt="{chart_dir}/{chart}">'
                if job_url:
                    html += f'<a href="{job_url}/artifacts/file/{chart_dir}/{chart}">'
                    html += img
                    html += "</a>"
                else:
                    html += img
                html += "\n"
    with open("index.html", "w", encoding="utf-8") as f:
        f.write(HTML_BEGIN + html + HTML_END)
    if "CI_JOB_ID" in os.environ:
        print(
            "https://isc-projects.gitlab-pages.isc.org/-/"
            f"bind9-shotgun-ci/-/jobs/{os.environ['CI_JOB_ID']}/artifacts/index.html"
        )


if __name__ == "__main__":
    main()
